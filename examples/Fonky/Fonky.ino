#include "fonky_system.h"

System *mySystem;
void setup() {
  mySystem = new System();
}

void loop() {
  mySystem->run();
}