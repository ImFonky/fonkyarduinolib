#include "fonky_system.h"
#include "controllers/fonky_panasoniccontroller.h"
#include "cameras/PTZCameras/fonky_panasoniccamera.h"

System::System(ThreadController *threadMaster) {


#if SERIAL_DEBUG
    Serial.begin(38400);
#endif

    if(threadMaster == nullptr){
        threadMaster = new ThreadController();
    }

    this->threadMaster = threadMaster;

    delay(1000);

    network = new Network(threadMaster);
     delay(1000);

    cameras[KESSLER_BLACKMAGIC_PTZ] = new KesslerCraneBlackMagicCamera(threadMaster);
    cameras[PANASONIC_PTZ] = new PanasonicCamera(network);
    controllers[DEVICE_CONTROLLER] = new DeviceController(cameras[PANASONIC_PTZ], DEVICE_CONTROLLER);
    controllers[PANASONIC_CONTROLLER] = new PanasonicController(cameras[KESSLER_BLACKMAGIC_PTZ], PANASONIC_CONTROLLER,
                                                                network);

    Menu *myMenu = new Menu(this);

    network->start();
}

void System::run(){

    threadMaster->run();
}