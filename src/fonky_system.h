#ifndef FONKYSYSTEM_H
#define FONKYSYSTEM_H

/*!
 * @file fonky_system.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MAIN
 * @class System
 * @brief This class is the main class. It handles threads and share resources between actors of the system.
 */


#include "Arduino.h"

#define SYSTEM_NB_CAMERAS 2
#define SYSTEM_NB_CONTROLLERS 2

#include "utils/fonky_utils.h"
#include "cameras/PTZCameras/kesslercrane_blackmagic_camera.h"
#include "menu/fonky_menu.h"
#include "network/fonky_network.h"
#include "controllers/fonky_devicecontroller.h"

#include <Thread.h>
#include <ThreadController.h>

class System {

public:

    //! Defines all the types of camera currently handled by the system
    enum CameraTypes{KESSLER_BLACKMAGIC_PTZ, PANASONIC_PTZ};
    //! Defines all the types of controller currently handled by the system
    enum ControllerTypes{DEVICE_CONTROLLER, PANASONIC_CONTROLLER};

    //! This thread is scheduling all the other threads in the system
    ThreadController *threadMaster;

    //! Contains all the controllers in the system
    Controller *    controllers[SYSTEM_NB_CONTROLLERS];
    //! Contains all the controllers in the system
    Camera *cameras[SYSTEM_NB_CAMERAS];

    //! This shared object is used by all network dependant actors
    Network *network;

    /*! System constructor
      @param master (optional) a pointer to an external scheduler
    */
    System(ThreadController * master= nullptr);

    //! This method should be called in a loop, typically the void loop() function in arduino sketches (*.ino files).
    void run();
};

#endif