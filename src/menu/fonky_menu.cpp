#include "fonky_menu.h"

TouchScreen *Menu::screen;
System *Menu::system;

Node  *Menu::currentNode;
RootMenuNode *Menu::rootNode;
bool Menu::displayed;

void Menu::setup_screen() {
    TFT_BL_ON;      // turn on the background light
    Tft.TFTinit();  // init TFT library
    screen = new TouchScreen(XP, YP, XM, YM);
    delay(100);
}

void Menu::setup_menu() {
    rootNode = new RootMenuNode(system, "Menu");
    currentNode = rootNode;
    displayed = false;
    touchTicks = 0;
    launchTasks();
}

Thread Menu::menuThread;
unsigned short Menu::touchTicks;

void Menu::DisplayMenu(){
    CheckTouch();
    if (!displayed) {
        touchTicks = 0;
        displayed = true;
        if (currentNode != nullptr) {
            currentNode->displayNode(RED);
        }
    }
};

void Menu::CheckTouch() {

    ++touchTicks;

    if (touchTicks >= MAX_TOUCH_TICKS) {
        Point p;

        // a point object holds x y and z coordinates.
        p = screen->getPoint();

        //map the ADC value read to into pixel co-ordinates

        p.x = map(p.x, TS_MINX, TS_MAXX, 0, TFT_SCREEN_WIDTH);
        p.y = map(p.y, TS_MINY, TS_MAXY, 0, TFT_SCREEN_HEIGHT);

        // we have some minimum pressure we consider 'valid'
        // pressure of 0 means no pressing!

        if (p.z > __PRESURE) {
            // Check all clickable zone on the current node

            //currentNode->serialNode();

            //SERIAL_PRINTLN("Pression");

            Node *newNode;
            if ((newNode = currentNode->checkForClick(&p, currentNode)) != nullptr) {
                displayed = false;
                currentNode = newNode;
                touchTicks = 0;
            }
        } else {
            currentNode->releaseClick();
        }
    }
}

void Menu::launchTasks() {

    menuThread.onRun(DisplayMenu);
    menuThread.setInterval(25);
    system->threadMaster->add(&menuThread);

}

Menu::Menu(){
    setup_screen();
    setup_menu();
}

Menu::Menu(System *sys) {
    system = sys;
    setup_screen();
    setup_menu();
}

#ifdef NO_ARDUINO
Menu::~Menu(){
   delete(currentNode);
   delete(rootNode);
}
#endif