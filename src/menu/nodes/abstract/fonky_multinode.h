#ifndef FONKY_MULTINODE_H
#define FONKY_MULTINODE_H

/*!
 * @file fonky_multinode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section ABSTRACT
 * @class MultiNode
 * @brief Abstract templated class that defines the basic structure of a list display node.
 * This class is to be specialized with known value types. Used to make the very usefull MenuNode.
 * @see MenuNode
 */

#include "../abstract/fonky_node.h"

#define MULTINODE_CALC_MAX_DISP(yMin,yMax,ySize) ((yMax - yMin + ySize) / ySize)

#define MULTINODE_BUTTON_SIZE    NODE_BUTTON_SIZE
#define MULTINODE_X_MAX          (TFT_SCREEN_WIDTH - MULTINODE_BUTTON_SIZE)
#define MULTINODE_Y_MIN          MULTINODE_BUTTON_SIZE
#define MULTINODE_Y_MAX          (TFT_SCREEN_HEIGHT - MULTINODE_BUTTON_SIZE)
#define MULTINODE_MAX_DISP_NODES MULTINODE_CALC_MAX_DISP(MULTINODE_Y_MIN,MULTINODE_Y_MAX,MULTINODE_BUTTON_SIZE)
#define MULTINODE_MAX_CHILDRENS  15

template<class T>
class MultiNode : public Node {
protected:
    //! Page up handler
    static const ButtonFunction onClickUpButton;
    //! Page down handler
    static const ButtonFunction onClickDownButton;
    //! Enables the buttons corresponding to the item in the page
    static const ButtonCondition enableChildren;

    //! The UP button
    static Button upButton;
    //! The DOWN button
    static Button downButton;

    //! The y minimal bound (can vary in various specialisations of the class)
    unsigned short yMin;

    //! The items in the list
    T *childrens[MULTINODE_MAX_CHILDRENS];
    //! The number of item in the list
    unsigned short nbChildrens;
    //! The last clicked item index
    unsigned short index;
    //! The page index
    unsigned short page;
    //! Calculated number of pages in the node
    unsigned short maxPages;
    //! Signal if UP/DOWN buttons where already created
    bool controlsDisplayed;

public:
    /*! MultiNode constructor
     * @param name the name of the Node
     * @param yMin the minimal height to begin the list
     */
    MultiNode(const char *name = "Menu", unsigned short yMin=MULTINODE_Y_MIN);

    /*! This functions allows to add child items in the list
     * @param child the item to add in the node
     * @param father the container node
     * @param childName the displayed name of the child
     * @param onChildClick the action to do when clicking on an child item
     * @return Return true if the children was added and false if there is no remaining space
     */
    static bool addChildren(T *child, MultiNode<T> *father, const char* childName, ButtonFunctionPtr onChildClick=&onClickChildren);

    static T **getChildrens(MultiNode<T> *father);

    unsigned short getNbChildrens(MultiNode<T> *father);

    /*! This function defines witch child is targeted by the event
     * @param p the position where de click event occured
     * @param currentNode the displayed node when event occured
     * @return Return true if the event position was on the list and false for the other positions
     */
    static const bool onClickChildren(const Point *p, Node *currentNode);

#ifdef NO_ARDUINO
    ~MultiNode();
#endif
};

#include "fonky_multinode.tpp"

#endif //FONKY_MULTINODE_H
