#include <string.h>
#include "fonky_togglevaluemenunode.h"

Node *ToggleValueMenuNode::onToggleValue(const Point *p, Node *currentNode, Button *b) {
    ToggleValueMenuNode *n = (ToggleValueMenuNode *) currentNode;
    if (SelectionNode::onClickChildren(p, currentNode)) {
        n->setValue(b->childIndex, !n->getValue(b->childIndex));
        return currentNode;
    }
    return nullptr;
}


void ToggleValueMenuNode::displayButton(Button *btn) {
    if (btn->enableCondition(btn, this)) {
        unsigned short fontColor;

        if (strcmp(btn->text, "BACK") && strcmp(btn->text, "UP") && strcmp(btn->text, "DOWN")) {
            fontColor = getValue(btn->childIndex) ? GREEN : RED;
        } else {
            fontColor = BLACK;
        }

        Tft.fillRectangle(btn->x, btn->y, btn->width, btn->height, btn->backColor);
        Tft.drawString(btn->text,
                       centerXtext(btn->text, btn->x, btn->width, btn->textSize),
                       centerYtext(btn->y, btn->height, btn->textSize),
                       btn->textSize, fontColor);
    }
}

ToggleValueMenuNode::ToggleValueMenuNode(const char *name) : SelectionNode(name) {
    yMin = TOGGLENODE_Y_MIN;
    for (unsigned char i = 0; i < MULTINODE_MAX_CHILDRENS; ++i) {
        indexes[i] = i;
    }
}

void ToggleValueMenuNode::addChildren(const char *childName, ButtonFunctionPtr onChildClick) {

    SelectionNode::addChildren(&indexes[nbChildrens], childName, onChildClick);
}
