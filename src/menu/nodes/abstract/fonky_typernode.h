#ifndef FONKY_FONKY_TYPERNODE_H
#define FONKY_FONKY_TYPERNODE_H

/*!
 * @file fonky_typernode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section ABSTRACT
 * @class TyperNode
 * @brief Abstract Node that gives a template for value management nodes like keyboards or numpads
 */

#include "../abstract/fonky_node.h"

template<class T>
class TyperNode : public Node {
public :

    //! A pointer to the managed data
    T* valuesPtr;
    //! Number of data values
    unsigned short valueLength;

protected:
    //! Implented to make the class no more abstract. @see Node#checkClick
    Node *checkClick(const Point *p, Node *currentNode);

public:
    /*! TyperNode constructor
     * @param valuesPtr a pointer to the data
     * @param valueLength the number of data items
     * @param name the name of the Node
     */
    TyperNode(T* valuesPtr, unsigned short valueLength,
              const char *name = "Menu");

#ifdef NO_ARDUINO
    ~TyperNode();
#endif
};

/* Evil templates declaration/definition problem resolved like this */
#include "fonky_typernode.tpp"

#endif //FONKY_FONKY_TYPERNODE_H
