template<class T>
Node *MultiNode<T>::onClickUpButton(const Point *p, Node *currentNode, Button *b) {
    MultiNode *node = (MultiNode *) currentNode;
    node->page = (node->page - 1) % node->maxPages;
    return currentNode;
}

template<class T>
Node *MultiNode<T>::onClickDownButton(const Point *p, Node *currentNode, Button *b) {
    MultiNode *node = (MultiNode *) currentNode;
    node->page = (node->page + 1) % node->maxPages;
    return currentNode;
}

template<class T>
bool MultiNode<T>::enableChildren(Button *b, Node *currentNode) {
    MultiNode *n = (MultiNode *) currentNode;
    return ((b->childIndex % NODE_MAX_BUTTONS) >= (n->page * MULTINODE_MAX_DISP_NODES)) &&
           ((b->childIndex % NODE_MAX_BUTTONS) < ((n->page + 1) * MULTINODE_MAX_DISP_NODES));
}

template<class T>
Node::Button MultiNode<T>::upButton = {
        "UP",
        MULTINODE_X_MAX,
        MULTINODE_Y_MIN,
        MULTINODE_BUTTON_SIZE,
        MULTINODE_BUTTON_SIZE,
        BLACK,
        GREEN,
        2,
        &onClickUpButton,
        &defaultCondition,
        0
};

template<class T>
Node::Button MultiNode<T>::downButton = {
        "DOWN",
        MULTINODE_X_MAX,
        MULTINODE_Y_MAX,
        MULTINODE_BUTTON_SIZE,
        MULTINODE_BUTTON_SIZE,
        BLACK,
        GREEN,
        2,
        &onClickDownButton,
        &defaultCondition,
        0
};

template<class T>
MultiNode<T>::MultiNode(const char *n, unsigned short yMin):yMin(yMin){
    index = 0;
    nbChildrens = 0;
    page = 0;
    maxPages = 1;
    controlsDisplayed = false;
    initNode(n);
}

template<class T>
bool MultiNode<T>::addChildren(T *child, MultiNode<T>* f, const char* childName, ButtonFunctionPtr onChildClick) {

    SERIAL_PRINT(F("Adding children "));
    SERIAL_PRINT(childName);
    SERIAL_PRINT(F(" -> "));
    SERIAL_PRINTLN(f->nbChildrens);


    if (f->nbChildrens < MULTINODE_MAX_CHILDRENS) {
        f->childrens[f->nbChildrens] = child;

        unsigned short yButton = ((f->nbChildrens * MULTINODE_BUTTON_SIZE) % MULTINODE_Y_MAX) + f->yMin;
        makeButton(f,childName,
                   0, yButton,
                   onChildClick, &enableChildren,
                   f->nbChildrens,
                   MULTINODE_X_MAX, MULTINODE_BUTTON_SIZE,
                   YELLOW, BLACK,
                   1);


        if ((f->nbChildrens > 0) && ((f->nbChildrens % MULTINODE_MAX_DISP_NODES) == 0)) {
            f->maxPages += 1;
        }

        if (!f->controlsDisplayed && f->maxPages == 2) {
            f->controlsDisplayed = true;
            makeButton(&upButton, f);
            makeButton(&downButton, f);
        }

        f->nbChildrens += 1;
        return true;
    } else {
        return false;
    }
}

template<class T>
T **MultiNode<T>::getChildrens(MultiNode <T> *father) {
    return father->childrens;
}

template<class T>
unsigned short MultiNode<T>::getNbChildrens(MultiNode <T> *father) {
    return father->nbChildrens;
}

template<class T>
const bool MultiNode<T>::onClickChildren(const Point *p, Node *currentNode) {
    MultiNode<T> *node = (MultiNode<T> *) currentNode;
    if (p->x < MULTINODE_X_MAX && p->y >= node->yMin) {
        unsigned short index = (p->y / MULTINODE_BUTTON_SIZE) - (node->yMin / MULTINODE_BUTTON_SIZE);
        unsigned char maxDispChildren = MULTINODE_CALC_MAX_DISP(node->yMin,MULTINODE_Y_MAX,MULTINODE_BUTTON_SIZE);
        index += node->page * maxDispChildren;
        index = index < MULTINODE_MAX_CHILDRENS ? index : MULTINODE_MAX_CHILDRENS;
        node->index = index;
        return true;
    }
    return false;
}

#ifdef NO_ARDUINO

template<class T>
MultiNode::~MultiNode(){
    for(unsigned short i = 0; i < nbChildrens; ++i){
        delete(childrens[i]);
    }
}
#endif
