#include "fonky_numpadnode.h"

const char* NumpadNode::numpadStrKeys[NUMNODE_NB_KEYS] = {"0","1","2","3","4","5","6","7","8","9"};

NumpadNode::NumpadNode(unsigned char* vals, unsigned short vL, ButtonFunctionPtr onClickKey, const char *name)
        :TyperNode(vals, vL, name){

    unsigned short x,y;
    for(unsigned short i = 0; i < NUMNODE_NB_KEYS; ++i){
        x = (i * NUMNODE_KEY_WIDTH) % NUMNODE_LINE_WIDTH;
        y = ((i / NUMNODE_KEYS_PER_LINE) * NUMNODE_KEY_HEIGHT) + NUMNODE_Y_MIN_KEYS;
        makeButton(this, numpadStrKeys[i], x, y, onClickKey, &defaultCondition, i, NUMNODE_KEY_WIDTH, NUMNODE_KEY_HEIGHT);
    }

}

