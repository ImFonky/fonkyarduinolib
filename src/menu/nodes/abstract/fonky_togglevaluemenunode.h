//
// Created by pbmon on 11.07.2018.
//

#ifndef FONKY_TOGGLEVALUEMENU_H
#define FONKY_TOGGLEVALUEMENU_H

/*!
 * @file fonky_togglevaluemenunode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section ABSTRACT
 * @class ToggleValueMenuNode
 * @brief Abstract MenuNode that offers a template for créating item toggling menus
 */

#include "../menunodes/fonky_selectionnode.h"

#define TOGGLENODE_Y_MIN          (NODE_BUTTON_SIZE)


class ToggleValueMenuNode : public SelectionNode {
public:
    //! The functions that handle toggling
    static const ButtonFunction onToggleValue;

protected:

    //! The button indexes of each childrens
    unsigned char indexes[NODE_MAX_BUTTONS];

    //! Override the base method to display buttons with red or green color, depending on their state
    void displayButton(Button *btn);

public:
    /*! ToggleValueMenuNode constructor
     * @param name the name of the Node
     */
    ToggleValueMenuNode(const char *name = "Menu");

    //! Override the base method to add specific toggling function when click on children
    void addChildren(const char *childName, ButtonFunctionPtr onChildClick = &onToggleValue);

    virtual bool getValue(unsigned short index) = 0;

    virtual void setValue(unsigned short index, bool value) = 0;

#ifdef NO_ARDUINO
    ~SelectionNode();
#endif

};


#endif //FONKY_FONKY_TOGGLEVALUEMENU_H
