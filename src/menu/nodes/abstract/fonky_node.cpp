//
// Created by pbmon on 07.07.2018.
//

#include "fonky_node.h"

Node *Node::defaultOnClick(const Point *p, Node *currentNode, Button *b) {
#if SERIAL_PRINT
    //SERIAL_PRINTLN("Default onClick called");
#endif
    return nullptr;
}

bool Node::defaultCondition(Button *b, Node *currentNode) {
    //SERIAL_PRINTLN("Default condition called");
    return true;
}

Node *Node::defaultOnClickBackButton(const Point *p, Node *currentNode, Button *b) {
    //SERIAL_PRINTLN("BackButton onClick called");
    return currentNode->father;
}

unsigned short
Node::centerXtext(const char *text, unsigned short x, unsigned short width, unsigned short textSize) {
    unsigned short textXsize, textX;
    textXsize = textSize * 6.5;
    textX = x + (int) (width - textXsize * strlen(text)) / 2;
    textX = textX <= (x + width / 2) ? textX : 0;
    return textX;
}

unsigned short Node::centerYtext(unsigned short y, unsigned short height, unsigned short textSize) {
    unsigned short textYsize, textY;
    textYsize = textSize * 6.5;
    textY = y + (int) (height - textYsize) / 2;
    textY = textY <= (y + height / 2) ? textY : 0;
    return textY;
}

static int nbButtoons = 0;

#if STATIC_ALLOC
Node::Button* Node::setupButton(Button* btn, const char *text,
                                unsigned short x, unsigned short y,
                                ButtonFunctionPtr onClick, ButtonConditionPtr enableCondition,
                                unsigned short childIndex,
                                unsigned short width, unsigned short height,
                                unsigned short fontColor, unsigned short backColor,
                                unsigned short textSize) {


    SERIAL_PRINT(F("Button "));
    SERIAL_PRINT(text);
    SERIAL_PRINT(F(" --> "));
    SERIAL_PRINT(nbButtoons * sizeof(Button));
    SERIAL_PRINT(F(" --> Add : "));
    Serial.print((unsigned int)&btn);

    nbButtoons++;

    btn->text = text;
    btn->x = x;
    btn->y = y;
    btn->onClick = onClick;
    btn->enableCondition = enableCondition;
    btn->width = width;
    btn->height = height;
    btn->fontColor = fontColor;
    btn->backColor = backColor;
    btn->textSize = textSize;
    btn->childIndex = childIndex;
    return btn;
}

Node::Button* Node::setupButton(Button* dest, Button* src){
    return setupButton(dest,src->text,src->x,src->y,src->onClick,src->enableCondition,src->childIndex,src->width,src->height,src->fontColor,src->backColor,src->textSize);
}


#else
Node::Button* Node::setupButton(const char *text,
                                            unsigned short x, unsigned short y,
                                            ButtonFunctionPtr onClick, ButtonConditionPtr enableCondition,
                                            unsigned short childIndex,
                                            unsigned short width, unsigned short height,
                                            unsigned short fontColor, unsigned short backColor,
                                            unsigned short textSize) {

    Button* btn = (Button*)malloc(sizeof(Button));

    nbButtoons++;

    btn->text = text;
    btn->x = x;
    btn->y = y;
    btn->onClick = onClick;
    btn->enableCondition = enableCondition;
    btn->width = width;
    btn->height = height;
    btn->fontColor = fontColor;
    btn->backColor = backColor;
    btn->textSize = textSize;
    btn->childIndex = childIndex;
    return btn;
}
#endif

void Node::_displayButton(Button *btn, Node *node) {
    if (btn->enableCondition(btn, node)) {
        Tft.fillRectangle(btn->x, btn->y, btn->width, btn->height, btn->backColor);
        Tft.drawString(btn->text,
                       centerXtext(btn->text, btn->x, btn->width, btn->textSize),
                       centerYtext(btn->y, btn->height, btn->textSize),
                       btn->textSize, btn->fontColor);
    }
}

void Node::displayButton(Button *btn) {
    _displayButton(btn, this);
}


#if STATIC_ALLOC
void Node::makeButton(Node* node, const char *text,
                            unsigned short x, unsigned short y,
                            ButtonFunctionPtr onClick, ButtonConditionPtr enableCondition,
                            unsigned short childIndex,
                            unsigned short width, unsigned short height,
                            unsigned short fontColor, unsigned short backColor,
                            unsigned short textSize) {

    if (node->nbButtons < NODE_MAX_BUTTONS) {
        setupButton(&node->registeredButtons[node->nbButtons],text, x, y, onClick, enableCondition, childIndex, width, height,
                                                               fontColor, backColor, textSize);
        node->nbButtons += 1;
        SERIAL_PRINT(F(" --> Sys : "));
        SERIAL_PRINTLN((unsigned int)node);

    } else {
        SERIAL_PRINTLN(F("No more space for button"));
    }
}

#else
void Node::makeButton(Node* node, const char *text,
                            unsigned short x, unsigned short y,
                            ButtonFunctionPtr onClick, ButtonConditionPtr enableCondition,
                            unsigned short childIndex,
                            unsigned short width, unsigned short height,
                            unsigned short fontColor, unsigned short backColor,
                            unsigned short textSize) {

    if (node->nbButtons < NODE_MAX_BUTTONS) {
        node->registeredButtons[node->nbButtons] = setupButton(text, x, y, onClick, enableCondition, childIndex, width, height,
                                                               fontColor, backColor, textSize);
        node->nbButtons += 1;
        SERIAL_PRINT(F(" --> Sys : "));
        SERIAL_PRINTLN((unsigned int)node);

    } else {
        SERIAL_PRINTLN(F("No more space for button"));
    }
}
#endif

void Node::makeBackButton(Node* node, const ButtonFunctionPtr onClick) {
    makeButton(node, "BACK", NODE_X_MAX, 0, onClick);
}

#if STATIC_ALLOC
void Node::makeButton(Button* btn, Node* node) {

    if (node->nbButtons < NODE_MAX_BUTTONS) {
        setupButton(&node->registeredButtons[node->nbButtons],btn);
        node->nbButtons += 1;
    } else {
        SERIAL_PRINTLN(F("No more space for button"));
    }
}

#else

void Node::makeButton(Button *btn, Node *node) {

    if (node->nbButtons < NODE_MAX_BUTTONS) {
        node->registeredButtons[node->nbButtons] = btn;
        node->nbButtons += 1;
    } else {
        SERIAL_PRINTLN(F("No more space for button"));
    }
}
#endif



void Node::initNode(const char *n) {
    name = n;
    father = nullptr;
    nbButtons = 0;
}

Node *Node::checkForClick(const Point *p, Node *currentNode) {
    Button *btn;
    for (unsigned short i = 0; i < nbButtons; ++i) {
#if STATIC_ALLOC
        btn = &registeredButtons[i];
#else
        btn = registeredButtons[i];
#endif
        if (p->x >= btn->x && p->x < (btn->x + btn->width) &&
            p->y >= btn->y && p->y < (btn->y + btn->height)) {
            return btn->onClick(p, currentNode, btn);
        }
    }

    return checkClick(p, currentNode);
}

void Node::releaseClick() {}


void Node::setFather(Node* current, Node *father, Node::ButtonFunctionPtr backButtonFunction) {
    if(current->father == nullptr){
        makeBackButton(current,backButtonFunction);
    }
    current->father = father;
}

void Node::displayNode(unsigned short color) {

    Tft.fillRectangle(0, 0, TFT_SCREEN_WIDTH, TFT_SCREEN_HEIGHT, BLACK);

    display();

    Tft.drawString(name,
                   centerXtext(name, 0, NODE_X_MAX, 3),
                   centerYtext(0, NODE_BUTTON_SIZE, 3),
                   3, color);

    for (unsigned short i = 0; i < nbButtons; ++i) {
#if STATIC_ALLOC
        displayButton(&registeredButtons[i]);
#else
        displayButton(registeredButtons[i]);
#endif

    }
}

const char *Node::getName() { return name; }

Node *Node::getFather() { return father; }


#ifdef NO_ARDUINO
Node::~Node(){}
#endif