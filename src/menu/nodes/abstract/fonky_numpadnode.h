#ifndef FONKY_FONKY_NUMPADNODE_H
#define FONKY_FONKY_NUMPADNODE_H

/*!
 * @file fonky_numpadnode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section ABSTRACT
 * @class NumpadNode
 * @brief Abstract Node that gives a template for numeric field management.
 * This class implements a numeric keyboard that shall be used in children classes.
 */

#include "fonky_typernode.h"

#define NUMNODE_NB_KEYS         10
#define NUMNODE_KEYS_PER_LINE   5
#define NUMNODE_KEY_WIDTH       (TFT_SCREEN_WIDTH / NUMNODE_KEYS_PER_LINE)
#define NUMNODE_LINE_WIDTH      TFT_SCREEN_WIDTH
#define NUMNODE_KEY_HEIGHT      NUMNODE_KEY_WIDTH
#define NUMNODE_Y_MIN_KEYS      (TFT_SCREEN_HEIGHT - ((NUMNODE_NB_KEYS / NUMNODE_KEYS_PER_LINE) * NUMNODE_KEY_HEIGHT))
#define NUMNODE_Y_MIN_CONTROLS  (NUMNODE_Y_MIN_KEYS - NUMNODE_KEY_HEIGHT)
#define NUMNODE_X_MIN_CONTROLS  (TFT_SCREEN_WIDTH - NUMNODE_KEY_WIDTH)
#define NUMNODE_Y_MIN           (MENUNODE_BUTTON_SIZE * 2)

class NumpadNode : public TyperNode<unsigned char> {
protected:
    //! Handles the click function for the keys of the numpad
    static const char* numpadStrKeys[NUMNODE_NB_KEYS];

public:
    /*! ToggleValueMenuNode constructor
     * @param valuesPtr a pointer to the data
     * @param valueLength the number of items in the data
     * @param onClickKey the numpad onKeyClick handler
     * @param name the name of the Node
     */
    NumpadNode(unsigned char* valuesPtr, unsigned short valueLength,
               ButtonFunctionPtr onClickKey,
               const char *name = "Menu");

#ifdef NO_ARDUINO
    ~NumpadNode();
#endif
};

#endif //FONKY_FONKY_NUMPADNODE_H
