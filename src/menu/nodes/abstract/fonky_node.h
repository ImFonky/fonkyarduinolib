#ifndef FONKY_NODE_H
#define FONKY_NODE_H

/*!
 * @file fonky_node.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section ABSTRACT
 * @class Node
 * @brief Abstract class that defines the basic structure of every nodes in the system
 * The node is responsible for displaying itself and handling clicks on it. It works like a webpage or a window.
 * The node can have a father, in witch case a BACK button will be automatically added at the top right of the page
 */

#include <Arduino.h>
#include <TFTv2.h>

#include "../../../utils/fonky_seeed_touch_screen.h"
#include "../../../utils/fonky_utils.h"


#define TFT_SCREEN_WIDTH 240
#define TFT_SCREEN_HEIGHT 320

#define NODE_BUTTON_SIZE        (TFT_SCREEN_HEIGHT / 5)
#define NODE_MAX_BUTTONS 16

#define NODE_X_MAX          (TFT_SCREEN_WIDTH - NODE_BUTTON_SIZE)

class Node {
protected:
    struct St_Button;

    //! Defines a function type to be called when a button is released
    typedef Node *(ReleaseFunction)(const Point *p, Node *currentNode, struct St_Button *b);

    //! Defines a pointer to the ReleaseFunction
    typedef ReleaseFunction *ReleaseFunctionPtr;

    //! Defines special function type that allows easy handler management and creation
    typedef Node *(ButtonFunction)(const Point *p, Node *currentNode, struct St_Button *b);

    //! Defines a pointer to the ButtonFunction
    typedef ButtonFunction *ButtonFunctionPtr;

    //! Defines condition function type to handle button visibility
    typedef bool (ButtonCondition)(struct St_Button *b, Node *currentNode);

    //! Defines a pointer to the ButtonCondition
    typedef ButtonCondition *ButtonConditionPtr;

    //! Defines a button structure containing size, position and value parameters and pointers to handler functions
    typedef struct St_Button {
        const char *text;
        unsigned short x;
        unsigned short y;
        unsigned short width;
        unsigned short height;
        unsigned short fontColor;
        unsigned short backColor;
        unsigned short textSize;
        ButtonFunctionPtr onClick;
        ButtonConditionPtr enableCondition;
        unsigned short childIndex;
    } Button;

#if STATIC_ALLOC
    static Button* setupButton(Button* btn, const char *text,
                               unsigned short x, unsigned short y,
                               ButtonFunctionPtr onClick, ButtonConditionPtr enableCondition,
                               unsigned short childIndex,
                               unsigned short width = NODE_BUTTON_SIZE, unsigned short height = NODE_BUTTON_SIZE,
                               unsigned short fontColor = BLACK, unsigned short backColor = GREEN,
                               unsigned short textSize = 2);

    static Button* setupButton(Button* dest, Button* src);

#else

    /*! This functions allows to create and allocate a new button
     * @param text the displayed value of the button
     * @param x the horizontal postion
     * @param y the vertical postion
     * @param onClick the button click handler
     * @param enableCondition the name of the Node the button enable condition handler
     * @param childIndex the id of the children in a MultiNode
     * @param width the width of the button
     * @param height the height of the button
     * @param fontColor the text color
     * @param backColor the background color
     * @param textSize the size of the text. Values 1,2 or 3 may be used but bigger font is useless
     * @return Return a Button object pointer
     */
    static Button* setupButton(const char *text,
                               unsigned short x, unsigned short y,
                               ButtonFunctionPtr onClick, ButtonConditionPtr enableCondition,
                               unsigned short childIndex,
                               unsigned short width = NODE_BUTTON_SIZE, unsigned short height = NODE_BUTTON_SIZE,
                               unsigned short fontColor = BLACK, unsigned short backColor = GREEN,
                               unsigned short textSize = 2);
#endif

    /*! This function devfines the usual way to display buttons
     * @param btn a pointer to the button
     * @param node the node in witch the button is called
     */
    static void _displayButton(Button *btn, Node *node);

    /*! This function just calls _displayButton. @see Node#_displayButton
     * @param btn a pointer to the button
     */
    virtual void displayButton(Button *btn);

    //! The father node is used to go back to the previous node
    Node *father;

    //! The node (displayed) name
    const char *name;

#if STATIC_ALLOC
    Button registeredButtons[NODE_MAX_BUTTONS];
#else
    //! All the buttons in the node
    Button *registeredButtons[NODE_MAX_BUTTONS];
#endif
    //! The current number of buttons in the node
    unsigned short nbButtons;

    //! Handles the default click on a button
    static const ButtonFunction defaultOnClick;
    //! Handles the default condition for enabling button (always true)
    static const ButtonCondition defaultCondition;
    //! Handles the default click on the BACK button
    static const ButtonFunction defaultOnClickBackButton;

    /*! This functions allows to create and store a button
     * @param node a pointer to the container node
     * @param text the displayed value of the button
     * @param x the horizontal postion
     * @param y the vertical postion
     * @param click the button click handler
     * @param enableCondition the name of the Node the button enable condition handler
     * @param childIndex the id of the children in a MultiNode
     * @param width the width of the button
     * @param height the height of the button
     * @param fontColor the text color
     * @param backColor the background color
     * @param textSize the size of the text. Values 1,2 or 3 may be used but bigger font is useless
     */
    static void makeButton(Node* node, const char *text,
                           unsigned short x, unsigned short y,
                           ButtonFunctionPtr click = &defaultOnClick,
                           ButtonConditionPtr enableCondition = &defaultCondition,
                           unsigned short childIndex = 0,
                           unsigned short width = NODE_BUTTON_SIZE, unsigned short height = NODE_BUTTON_SIZE,
                           unsigned short fontColor = BLACK, unsigned short backColor = GREEN,
                           unsigned short textSize = 2);

    /*! This functions allows quickly create a BACK button and bind the related handler
     * @param node a pointer to the container node
     * @param onClick the backClick handler
     */
    static void makeBackButton(Node* node, ButtonFunctionPtr onClick=&defaultOnClickBackButton);

    /*! This functions allows store an already existing button in a node
     * @param btn a pointer to the button to store
     * @param node a pointer to the containing node
     */
    static void makeButton(Button *btn, Node *node);

    /*! Initializes node values.
     * Shall be called in the constructor of each class that inherit from Node.
     * @param name the name of the new node
     */
    void initNode(const char *name = "Menu");

    /*! Defines a virtual method that have to be implemented in each children classes.
     * This method will be called each time a node is displayed. It will contain custom elements for specific nodes displays.
     */
    virtual void display() = 0;

    /*! Defines a virtual method that have to be implemented in each children classes.
     * This method will be called each time a pressure is detected on the screen
     * @param name the name of the new node
     */
    virtual Node *checkClick(const Point *p, Node *newNode) = 0;

public:
    /*! Defines a virtual method that handle click release.
     * Can be overriden.
     */
    virtual void releaseClick();

    /*! Calculates the absolute horizontal position of a centered text
     * @param text the text to be displayed
     * @param x the starting position of the centred zone
     * @param width the width of the centered zone
     * @param textSize the size of the font
     * @return Returns the centered x initial position of the text
     */
    static unsigned short
    centerXtext(const char *text, unsigned short x, unsigned short width, unsigned short textSize = 2);

    /*! Calculates the absolute vertical position of a centered text
     * @param y the starting position of the centred zone
     * @param height the height of the centered zone
     * @param textSize the size of the font
     * @return Returns the centered y initial position of the text
     */
    static unsigned short centerYtext(unsigned short y, unsigned short height, unsigned short textSize = 2);

    /*! This function manages to set the father and make a BACK button
     * @param current the children node
     * @param father the father node
     * @param backButtonFunction the function that handle the BACK event
     */
    static void setFather(Node* current, Node *father, ButtonFunctionPtr backButtonFunction);

    /*! This function is the generic click checker.
     * This method checks for each buttons in the node if they have been clicked. Once done, it calls the checkClick
     * method of the specialized class
     * @param p the position of the click event
     * @param currentNode the current displayed node
     * @return Return nullptr if the page doesnt need refresh. Return currenNode if the same page have to be refreshed. Return a new node if the page have to be changed.
     */
    Node *checkForClick(const Point *p, Node *currentNode);

    /*! This function displays the shared part of nodes like buttons and name.
     * @param color The font color of the node title
     */
    void displayNode(unsigned short color);

    const char *getName();

    Node *getFather();

#ifdef NO_ARDUINO
    virtual ~Node() = 0;
#endif
};


#endif //FONKY_NODE_H
