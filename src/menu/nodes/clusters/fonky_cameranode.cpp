#include "fonky_cameranode.h"
#include "../abstract/fonky_togglevaluemenunode.h"
#include "../menunodes/fonky_toogleboundsnode.h"

CameraNode::CameraNode(System *system, Camera *ptz, const char *name) : MenuNode(name)
{

    currentController = &system->controllers[ptz->getControllerIndex()];

    this->addChildren(new ControllerSelectionNode(system, &currentController, "Controller selection"), &onClickChildren,
                      &ControllerSelectionNode::onSelectController);

    this->addChildren(new ToggleBoundsNode(ptz, false, "Toggle high bounds"),
                      &ToggleBoundsNode::onOpenBoundsSelection);
    this->addChildren(new ToggleBoundsNode(ptz, true, "Toggle low bounds"),
                      &ToggleBoundsNode::onOpenBoundsSelection);
}
