#ifndef FONKY_FONKY_ROOTMENUNODE_H
#define FONKY_FONKY_ROOTMENUNODE_H

/*!
 * @file fonky_rootmenunode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class RootMenuNode
 * @brief Implements a MenuNode and fill it with all system main functions.
 * This Node is basically to be used as the first menu entry. It is why it is called "root".
 */

#include "../menunodes/fonky_menunode.h"
#include "../../../cameras/abstract/fonky_camera_template.h"
#include "fonky_cameranode.h"
#include "../../../fonky_system.h"

class System;

class RootMenuNode : public MenuNode {
protected:
    //! A pointer to the system
    static System *system;

    /*! This function handles camera activation when clicking on one of the object's children
     * @param p the point where the click event occured
     * @param currentNode the current displayed node a the event time
     * @param b the button that triggered the event
     * @return Returns a Node if the display have to be updated or nullptr if the action dont require a GUI update
     */
    static Node *activateCamera(const Point *p, Node *currentNode, Button *b);

    static ButtonFunction onClickGetIp;
    static ButtonFunction onBackSetIp;

public:
    //! RootMenuNode default constructor
    RootMenuNode();

    /*! RootMenuNode constructor
     * @param system a pointer to the system
     * @param name the displayed name of the node
     */
    RootMenuNode(System *system, const char *name = "Menu");

#ifdef NO_ARDUINO
    ~RootMenuNode();
#endif
};

#endif //FONKY_FONKY_ROOTMENUNODE_H
