#ifndef FONKY_FONKY_CAMERANODE_H
#define FONKY_FONKY_CAMERANODE_H

/*!
 * @file fonky_cameranode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class CameraNode
 * @brief Menunode that initializes camera management children Nodes
 */

#include "../menunodes/fonky_menunode.h"
#include "../menunodes/fonky_selectionnode.h"
#include "../../../cameras/abstract/fonky_camera_template.h"
#include "../valuenodes/fonky_ipnumpadnode.h"
#include "../valuenodes/fonky_controllerselectionnode.h"
#include "../valuenodes/fonky_speedselectionnode.h"
#include "../custom/fonky_controllernode.h"
#include "../../../controllers/abstract/fonky_controller_template.h"
#include "../../../fonky_system.h"

class System;

class CameraNode: public MenuNode {
protected:
    //! Assigned controller for the managed camera
    Controller** currentController;

public:
    /*! CameraNode constructor
     * @param system a pointer to the system
     * @param ptzCamera the camera managed by this node
     * @param name the name of the Node
     */
    CameraNode(System *system, Camera *ptzCamera, const char *name = "Menu");

};


#endif //FONKY_FONKY_CAMERANODE_H
