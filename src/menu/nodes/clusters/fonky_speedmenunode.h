#ifndef FONKY_SPEEDMENUNODE_H
#define FONKY_SPEEDMENUNODE_H

/*!
 * @file fonky_speedmenunode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class SpeedMenuNode
 * @brief Implements a MenuNode and fill it with a children for each controller function
 */


#include "../menunodes/fonky_menunode.h"
#include "../../../controllers/abstract/fonky_controller_template.h"

class SpeedMenuNode : public MenuNode {
public:
    /*! WarningNode constructor
     * @param ctrl the managed controller
     * @param name the name of the Node
     */
    SpeedMenuNode(Controller *ctrl, const char *name);
};


#endif //FONKY_SPEEDMENUNODE_H
