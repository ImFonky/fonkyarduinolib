#ifndef FONKY_NETWORKCAMERANODE_H
#define FONKY_NETWORKCAMERANODE_H

/*!
 * @file fonky_networkcameranode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class NetworkCameraNode
 * @brief Same as CameraNode class but offers an additional node for camera ip selection
 * @see CameraNode
 */


#include "fonky_cameranode.h"
#include "../../../cameras/abstract/fonky_networkcamera_template.h"

class NetworkCameraNode : public CameraNode {
protected:
    NetworkCamera *networkCamera;
public:
    static ButtonFunction onClickGetIp;
    static ButtonFunction onBackSetIp;

    /*! NetworkCameraNode constructor
     * @param system a pointer to the system
     * @param ptz a pointer to the network camera object
     * @param name the displayed name of the node
     */
    NetworkCameraNode(System *system, NetworkCamera *netptz, const char *name = "Menu");
};


#endif //FONKY_NETWORKCAMERANODE_H
