#include "fonky_networkcameranode.h"

Node *NetworkCameraNode::onClickGetIp(const Point *p, Node *currentNode, Button *b) {
    IpNumpadNode *node = (IpNumpadNode *) ((RootMenuNode *) currentNode)->getChildrens(
            ((RootMenuNode *) currentNode))[b->childIndex];
    NetworkCameraNode *father = (NetworkCameraNode *) currentNode->getFather();
    node->ipAddress[0] = father->networkCamera->getIp()[0];
    node->ipAddress[1] = father->networkCamera->getIp()[1];
    node->ipAddress[2] = father->networkCamera->getIp()[2];
    node->ipAddress[3] = father->networkCamera->getIp()[3];
    return onClickChildren(p, currentNode, b);
}

Node *NetworkCameraNode::onBackSetIp(const Point *p, Node *currentNode, Button *b) {
    IpNumpadNode *node = (IpNumpadNode *) currentNode;
    NetworkCameraNode *father = (NetworkCameraNode *) currentNode->getFather();
    father->networkCamera->setIp((char *) node->ipAddress);
    return defaultOnClickBackButton(p, currentNode, b);
}

NetworkCameraNode::NetworkCameraNode(System *system, NetworkCamera *netPtz, const char *n) : CameraNode(system, netPtz,
                                                                                                        n) {
    networkCamera = netPtz;
    addChildren(new IpNumpadNode("Camera IP"), &onClickGetIp, &onBackSetIp);
}
