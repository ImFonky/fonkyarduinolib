//
// Created by pbmon on 24.07.2018.
//

#include "fonky_speedmenunode.h"
#include "../valuenodes/fonky_speedselectionnode.h"

SpeedMenuNode::SpeedMenuNode(Controller *ctrl, const char *name) : MenuNode(name) {
    addChildren(new SpeedSelectionNode(ctrl, Controller::PAN, "PAN speed"));
    addChildren(new SpeedSelectionNode(ctrl, Controller::TILT, "TILT speed"));
    addChildren(new SpeedSelectionNode(ctrl, Controller::ZOOM, "ZOOM speed"));
    addChildren(new SpeedSelectionNode(ctrl, Controller::FOCUS, "FOCUS speed"));
    addChildren(new SpeedSelectionNode(ctrl, Controller::IRIS, "IRIS speed"));
}
