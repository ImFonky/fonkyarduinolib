#include "fonky_rootmenunode.h"
#include "fonky_networkcameranode.h"
#include "fonky_speedmenunode.h"
#include "../custom/fonky_warningnode.h"
#include "../valuenodes/fonky_simpenumpadnode.h"
#include "../../../cameras/PTZCameras/fonky_panasoniccamera.h"

System *RootMenuNode::system;

Node *RootMenuNode::activateCamera(const Point *p, Node *currentNode, Button *b) {

    const char *inCaseOf = "Power up BMD 3G-SDI shield!";
    Tft.drawString(inCaseOf,
                   centerXtext(inCaseOf, 0, WARNINGNODE_TEXT_WIDTH, 1),
                   centerYtext(NODE_BUTTON_SIZE + WARNINGNODE_TEXT_HEIGHT, WARNINGNODE_TEXT_HEIGHT, 1),
                   1, GREEN);

    system->network->stop();
    ((KesslerCraneBlackMagicCamera *) (system->cameras[System::KESSLER_BLACKMAGIC_PTZ]))->activateCamera();
    return defaultOnClickBackButton(p, currentNode, b);
}

Node *RootMenuNode::onClickGetIp(const Point *p, Node *currentNode, Button *b) {
    IpNumpadNode *node = (IpNumpadNode *) ((RootMenuNode *) currentNode)->getChildrens(
            ((RootMenuNode *) currentNode))[b->childIndex];
    node->ipAddress[0] = system->network->getDeviceIp()[0];
    node->ipAddress[1] = system->network->getDeviceIp()[1];
    node->ipAddress[2] = system->network->getDeviceIp()[2];
    node->ipAddress[3] = system->network->getDeviceIp()[3];
    return onClickChildren(p, currentNode, b);
}

Node *RootMenuNode::onBackSetIp(const Point *p, Node *currentNode, Button *b) {
    IpNumpadNode *node = (IpNumpadNode *) currentNode;
    system->network->setDeviceIp(node->ipAddress);
    system->network->reset();
    return defaultOnClickBackButton(p, currentNode, b);
}

RootMenuNode::RootMenuNode(){}

RootMenuNode::RootMenuNode(System *sys, const char *n) : MenuNode(n) {
    system = sys;
    this->addChildren(new ControllerNode(system->controllers[System::DEVICE_CONTROLLER], "Controller"),&ControllerNode::controllerOnClickButton,&ControllerNode::controllerOnClickBackButton);
    DO_MEMORY_TEST();
    this->addChildren(new SpeedMenuNode(system->controllers[System::DEVICE_CONTROLLER], "Controller speeds"));
    DO_MEMORY_TEST();
    this->addChildren(new IpNumpadNode("Device IP"), &onClickGetIp, &onBackSetIp);
    DO_MEMORY_TEST();
    CameraNode *cameraNode = new CameraNode(system, system->cameras[System::KESSLER_BLACKMAGIC_PTZ],
                                            "Kessler/BlackMagic Camera");
    const char *warnings[WARNINGNODE_TEXT_NB_LINES] = {"WARNING!", "Network will be disabled", " until next reboot!"};
    cameraNode->addChildren(new WarningNode("Activate BMD Camera", warnings,
                                            &activateCamera));;
    cameraNode->addChildren(new SimpleNumpadNode(
            ((KesslerCraneBlackMagicCamera *) system->cameras[System::KESSLER_BLACKMAGIC_PTZ])->getCameraIdPtr(),
            "Camera id"));
    this->addChildren(cameraNode);

    DO_MEMORY_TEST();
    this->addChildren(new NetworkCameraNode(system, (PanasonicCamera *) system->cameras[System::PANASONIC_PTZ],
                                            "Panasonic Camera"));
    DO_MEMORY_TEST();
}


