#include "fonky_selectionnode.h"

Node *SelectionNode::onSelectValue(const Point *p, Node *currentNode, Button *b) {
    SelectionNode *n = (SelectionNode *) currentNode;
    if (MultiNode<Node>::onClickChildren(p, currentNode)) {
        return currentNode;
    }
    return nullptr;
}

void SelectionNode::display() {
    Tft.fillRectangle(SELNODE_BUTTON_SIZE, 0, SELNODE_X_MAX, SELNODE_BUTTON_SIZE, BLACK);
    if(index >= 0 && index < nbChildrens){
        Tft.drawString(strChildrens[index],
                       centerXtext(strChildrens[index], 0, SELNODE_X_MAX, 2),
                       centerYtext(SELNODE_BUTTON_SIZE, SELNODE_BUTTON_SIZE, 2),
                       2, CYAN);
    }
}

Node  *SelectionNode::checkClick(const Point *p, Node *currentNode) {
    return nullptr;
}

SelectionNode::SelectionNode(const char *name):MultiNode<unsigned char>(name,SELNODE_Y_MIN) {
}

void SelectionNode::addChildren(unsigned char *child, const char* childName, ButtonFunctionPtr onChildClick) {
    if(MultiNode<unsigned char>::addChildren(child, this, childName, onChildClick)){
        strChildrens[nbChildrens-1] = childName;
    }
}
