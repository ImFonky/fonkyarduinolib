#ifndef FONKY_MENUNODE_H
#define FONKY_MENUNODE_H

/*!
 * @file fonky_menunode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class MenuNode
 * @brief This class implements a simple menu Node that displays a list of Node childrens and allows to "enter" into them like directories.
 * If the number of children is bigger than the number of displayed items, buttons UP and DOWN will automatically be displayed at the right of the screen.
 */

#include "../abstract/fonky_multinode.h"

class MenuNode : public MultiNode<Node> {
public:
    //! Sets the Menu currentNode to the clicked children node @see Menu#currentNode
    static const ButtonFunction onClickChildren;
protected:

    //! Implements this class just to make SelectionNode not abstract @see Node#checkClick
    void display();

public:
    //! Implements this class just to make SelectionNode not abstract @see Node#checkClick
    Node *checkClick(const Point *p, Node *currentNode);

    /*! MenuNode constructor
     * @param name the name of the Node
     */
    MenuNode(const char *name = "Menu");

    /*! Specific add children function that sets specific action for the child click event
     * @param childName the displayed name othe the node
     * @param onChildClick the specific action to do when click on a child
     */
    void addChildren(Node *child, ButtonFunctionPtr onChildClick=&onClickChildren, const ButtonFunctionPtr backButton=&defaultOnClickBackButton);

#ifdef NO_ARDUINO
    ~MenuNode();
#endif
};


#endif //FONKY_MENUNODE_H
