#ifndef FONKY_FONKY_SELECTIONNODE_H
#define FONKY_FONKY_SELECTIONNODE_H

/*!
 * @file fonky_selectionnode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class SelectionNode
 * @brief This class implements a specilization of the MultiNode<T> to allow simple lists management.
 */


#include "../valuenodes/fonky_valuenode.h"
#include "../abstract/fonky_multinode.h"

#define SELNODE_BUTTON_SIZE    NODE_BUTTON_SIZE
#define SELNODE_X_MAX          MULTINODE_X_MAX
#define SELNODE_Y_MIN          (SELNODE_BUTTON_SIZE * 2)
#define SELNODE_Y_MAX          MULTINODE_Y_MAX
#define SELNODE_MAX_DISP_NODES MULTINODE_CALC_MAX_DISP(SELNODE_Y_MIN,SELNODE_Y_MAX,SELNODE_BUTTON_SIZE)
#define SELNODE_MAX_CHILDRENS   15

class SelectionNode : public MultiNode<unsigned char> {
public:
    //! On select value handler
    static const ButtonFunction onSelectValue;

protected:
    //! The displayed name of the Node childrens
    const char* strChildrens[MULTINODE_MAX_CHILDRENS];

    //! Displays the selected element value
    void display();

    //! Implements this class just to make SelectionNode not abstract @see Node#checkClick
    Node *checkClick(const Point *p, Node *newNode);

public:
    /*! SelectionNode constructor
     * @param name the name of the Node
     */
    SelectionNode(const char *name = "Menu");

    /*! Specific add children function that sets specific action for the child click event
     * @param childName the displayed name othe the node
     * @param onChildClick the specific action to do when click on a child
     */
    void addChildren(unsigned char *child, const char* childName, ButtonFunctionPtr onChildClick=&onSelectValue);

#ifdef NO_ARDUINO
    ~SelectionNode();
#endif

};



#endif //FONKY_FONKY_SELECTIONNODE_H
