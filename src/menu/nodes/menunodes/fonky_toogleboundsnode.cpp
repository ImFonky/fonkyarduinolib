#include "fonky_toogleboundsnode.h"
#include "fonky_menunode.h"

void ToggleBoundsNode::displayChildrens(ToggleBoundsNode *node) {

    for (unsigned char i = 0; i < /*node->childIndex*/ 2; ++i) {
        if (node->lowerBound) {
            if (node->camera->getLowerBound((Camera::Function) i) > LONG_MIN) {
                node->childButtons[i]->fontColor = RED;
            } else {
                node->childButtons[i]->fontColor = GREEN;
            }
//            Serial.println(node->camera->getLowerBound((Camera::Function) i));
        } else {
            if (node->camera->getUpperBound((Camera::Function) i) < LONG_MAX) {
                node->childButtons[i]->fontColor = RED;
            } else {
                node->childButtons[i]->fontColor = GREEN;
            }
//            Serial.println(node->camera->getUpperBound((Camera::Function) i));
        }
    }
}

Node *ToggleBoundsNode::onToggleChildren(const Point *p, Node *currentNode, Button *b) {
    ToggleBoundsNode *node = (ToggleBoundsNode *) currentNode;
//    ToggleBoundsNode *node = (ToggleBoundsNode*)(((MenuNode *)currentNode)->getChildrens((MenuNode *)currentNode)[b->childIndex]);


    Camera::Function fnc = (Camera::Function) b->childIndex;

//    Serial.println(node->getName());
//    Serial.println(fnc);


    if (node->lowerBound) {
        if (node->camera->getLowerBound(fnc) > LONG_MIN) {
            node->camera->unsetLowerBound(fnc);
        } else {
            node->camera->setLowerBound(fnc);
        }
    } else {
        if (node->camera->getUpperBound(fnc) < LONG_MAX) {
            node->camera->unsetUpperBound(fnc);
        } else {
            node->camera->setUpperBound(fnc);
        }
    }

    displayChildrens(node);
}

Node *ToggleBoundsNode::onOpenBoundsSelection(const Point *p, Node *currentNode, Button *b) {
//    ToggleBoundsNode *node = (ToggleBoundsNode*)(((MenuNode *)currentNode)->getChildrens((MenuNode *)currentNode)[b->childIndex]);
//
//    displayChildrens(node);

    return MenuNode::onClickChildren(p, currentNode, b);
}

ToggleBoundsNode::ToggleBoundsNode(Camera *cam, bool lower, const char *name) : ToggleValueMenuNode(name) {
    camera = cam;
    lowerBound = lower;
    childIndex = 0;
    for (unsigned char i = 0; i < CAMERA_NB_FUNCTIONS; ++i) {
        addChildren(Controller::functionNames[i]);
    }
}

bool ToggleBoundsNode::getValue(unsigned short index) {
    if (lowerBound) {
        return camera->getLowerBound((Camera::Function) index) != LONG_MIN;
    } else {
        return camera->getUpperBound((Camera::Function) index) != LONG_MAX;
    }
}

void ToggleBoundsNode::setValue(unsigned short index, bool lock) {
    if (lock) {
        if (lowerBound) {
            camera->setLowerBound((Camera::Function) index);
        } else {
            camera->setUpperBound((Camera::Function) index);
        }
    } else {
        if (lowerBound) {
            camera->unsetLowerBound((Camera::Function) index);
        } else {
            camera->unsetUpperBound((Camera::Function) index);
        }
    }
//    Serial.println(camera->getLowerBound((Camera::Function) index));
//    Serial.println(camera->getUpperBound((Camera::Function) index));
}

void ToggleBoundsNode::addChildren(const char *childName, ButtonFunctionPtr onChildClick) {
    childButtons[childIndex] = registeredButtons[nbButtons];
    childIndex++;
    ToggleValueMenuNode::addChildren(childName, onChildClick);
}


