#include "fonky_menunode.h"


Node *MenuNode::onClickChildren(const Point *p, Node *currentNode, Button *b) {
    MenuNode *n = (MenuNode *) currentNode;
    if (MultiNode<Node>::onClickChildren(p,currentNode)) {
        return n->childrens[n->index];
    }
    return nullptr;
}

void MenuNode::display() {
    // Everything is button in this node, nothing special to display
}

Node  *MenuNode::checkClick(const Point *p, Node *currentNode) {
    //If no button is found, nothing changes
    return nullptr;
}

MenuNode::MenuNode(const char *n):MultiNode<Node>(n){}

void MenuNode::addChildren(Node *child, ButtonFunctionPtr onChildClick, const ButtonFunctionPtr backButtonFunction){
    if(MultiNode<Node>::addChildren(child,this,child->getName(),onChildClick)){
        setFather(child,this,backButtonFunction);
    }
}


#ifdef NO_ARDUINO
MenuNode::~MenuNode(){
    for(unsigned short i = 0; i < nbChildrens; ++i){
        delete(childrens[i]);
    }
}
#endif
