#ifndef FONKY_TOOGLEBOUNDSNODE_H
#define FONKY_TOOGLEBOUNDSNODE_H

/*!
 * @file fonky_toggleboundsnode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class ToggleBoundsNode
 * @brief This class is specifically designed to display a menu for toggling PTZCamera Up or Low bounds.
 */


#include "../abstract/fonky_togglevaluemenunode.h"
#include "../../../controllers/abstract/fonky_controller_template.h"
#include "limits.h"

class Controller;

class ToggleBoundsNode : public ToggleValueMenuNode {
public:
    //! Defines what happen when click on bound
    static const ButtonFunction onToggleChildren;
    //! Defines what happen when opening the menu
    static const ButtonFunction onOpenBoundsSelection;

protected:
    //! Redefines menu childrens display in order to set RED or GREEN text color depending on bound state
    static void displayChildrens(ToggleBoundsNode *node);

    //! Stores separately general buttons and item buttons to apply specific display only on them
    Button *childButtons[NODE_MAX_BUTTONS];
    //! Number of items to toggle
    unsigned char childIndex;

    //! The camera managed by this menu
    Camera *camera;
    //! For witch camera function related bounds
    Camera::Function function;
    //! Is it lower or upper bound that is managed here?
    bool lowerBound;

public:
    /*! ToggleBoundsNode constructor
     * @param cam the camera to be managed
     * @param lowerBound witch bound is to be managed
     * @param name the name of the Node
     */
    ToggleBoundsNode(Camera *cam, bool lowerBound, const char *name);

    bool getValue(unsigned short index);
    void setValue(unsigned short index, bool value);

    /*! Specific add children function that sets specific action for the child click event
     * @param childName the displayed name othe the node
     * @param onChildClick the specific action to do when click on a child
     */
    void addChildren(const char *childName, ButtonFunctionPtr onChildClick = &ToggleValueMenuNode::onToggleValue);
};


#endif //FONKY_TOOGLEBOUNDSNODE_H
