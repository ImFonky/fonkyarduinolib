//
// Created by pbmon on 27.07.2018.
//

#include "fonky_simpenumpadnode.h"

Node *SimpleNumpadNode::onClickKey(const Point *p, Node *currentNode, Button *b) {
    SimpleNumpadNode *node = (SimpleNumpadNode *) currentNode;
    *(node->valuesPtr) = b->childIndex;
    return currentNode;
}

void SimpleNumpadNode::_display(SimpleNumpadNode *numNode) {
    char strValue[2] = {'0', 0};
    strValue[0] = (char) ('0' + (*(numNode->valuesPtr) % 10));

    Tft.drawString(strValue,
                   centerXtext(strValue, 0, TFT_SCREEN_WIDTH, 2),
                   centerYtext(SIMPLENODE_Y_MIN_VALUE, NODE_BUTTON_SIZE, 2),
                   2, CYAN);
}

void SimpleNumpadNode::display() {
    _display(this);
}

SimpleNumpadNode::SimpleNumpadNode(unsigned char *value, const char *name) : NumpadNode(value, 1, &onClickKey, name) {}