#ifndef FONKY_FONKY_IPNUMPADNODE_H
#define FONKY_FONKY_IPNUMPADNODE_H

/*!
 * @file fonky_ipnumpadnode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class IpNumpadNode
 * @brief This class is specifically designed to offer an IP address settings windows
 */

#include "../abstract/fonky_numpadnode.h"

#define IPNODE_Y_MIN_VALUE      NODE_BUTTON_SIZE
#define IPNODE_Y_MIN_HINT       (IPNODE_Y_MIN_VALUE + NUMNODE_KEY_HEIGHT)

class IpNumpadNode : public NumpadNode {
protected:

    //! Displays the IP adress
    static void _display(IpNumpadNode* ipNode);

    //! Calls _display. @see IpNumpadNode#_display
    void display();

    //! Numpad click handler
    static const ButtonFunction onClickKey;
    //! Next byte button handler
    static const ButtonFunction onClickNext;
    //! The index of the focused IP byte
    unsigned char byteSelector;

public:
    //! The node IP address
    unsigned char ipAddress[4] = {0, 0, 0, 0};

    /*! IpNumpadNode constructor
     * @param name the node name
     */
    IpNumpadNode(const char *name = "Menu");

#ifdef NO_ARDUINO
    ~IpNumpadNode();
#endif
};

#endif //FONKY_FONKY_IPNUMPADNODE_H
