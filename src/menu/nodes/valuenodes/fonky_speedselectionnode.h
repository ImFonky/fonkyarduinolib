#ifndef FONKY_FONKY_VALUESLIDERNODE_H
#define FONKY_FONKY_VALUESLIDERNODE_H

/*!
 * @file fonky_speedselectionnode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class SpeedSelectionNode
 * @brief This class is specifically designed to manage speed of controller that provide on/off motor control.
 * The device embedded controller @see DeviceController uses this menu to set the speed of each button
 */


#include "../abstract/fonky_typernode.h"
#include "../../../controllers/abstract/fonky_controller_template.h"

#define SLIDERNODE_KEY_WIDTH        NODE_BUTTON_SIZE
#define SLIDERNODE_KEY_HEIGHT       NODE_BUTTON_SIZE
#define SLIDERNODE_Y_MIN_CONTROLS   (TFT_SCREEN_HEIGHT - SLIDERNODE_KEY_HEIGHT)
#define SLIDERNODE_X_MIN_CONTROLS   (TFT_SCREEN_WIDTH - SLIDERNODE_KEY_WIDTH)
#define SLIDERNODE_Y_MIN_VALUE      NODE_BUTTON_SIZE
#define SLIDERNODE_Y_MIN_STEP       (SLIDERNODE_Y_MIN_VALUE + SLIDERNODE_KEY_HEIGHT)
#define SLIDERNODE_Y_MIN_HINT       (SLIDERNODE_Y_MIN_STEP + SLIDERNODE_KEY_HEIGHT)
#define SLIDERNODE_MAX_VALUE        100
#define SLIDERNODE_MIN_VALUE        0


class SpeedSelectionNode : public TyperNode<unsigned char> {
protected:
    //! Slider like event handler
    static const ButtonFunction onClickSlide;
    //! Increment modification step handler
    static const ButtonFunction onClickStep;

    //! Displays speed power and step
    static void _display(SpeedSelectionNode* valueNode);

    //! Calls _display @see SpeedSelectionNode#_display
    void display();

    //! Speed increment step
    unsigned char step;
    //! Speed power value
    unsigned char value;
    //! Managed controller
    Controller *controller;
    //! Managed controller function (PAN, TILT, ZOOM, FOCUS or IRIS)
    Controller::Function function;

public:
    /*! SpeedSelectionNode constructor
     * @param controller the controller to be managed
     * @param function the controller function to be managed
     * @param name the node name
     */
    SpeedSelectionNode(Controller *controller, Controller::Function function, const char *name = "Menu");

    unsigned char  getValue();
    static void setValue(SpeedSelectionNode *node, unsigned char value);

};


#endif //FONKY_FONKY_VALUESLIDERNODE_H
