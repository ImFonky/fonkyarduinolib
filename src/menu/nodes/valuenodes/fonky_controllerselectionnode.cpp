//
// Created by pbmon on 09.07.2018.
//

#include "fonky_controllerselectionnode.h"

Node *ControllerSelectionNode::onSelectController(const Point *p, Node *currentNode, Button *b) {
    ControllerSelectionNode* node = (ControllerSelectionNode*)currentNode;

    unsigned char tempIndex = (**node->menuController)->getCamera()->getControllerIndex();
    Camera *tempCam = node->system->controllers[node->index]->getCamera();

    node->system->controllers[tempIndex]->setCamera(tempCam);
    node->system->controllers[node->index]->setCamera((**node->menuController)->getCamera());
    *node->menuController = &node->system->controllers[node->index];
    return defaultOnClickBackButton(p,currentNode,b);
}

unsigned char ControllerSelectionNode::indexes[SYSTEM_NB_CONTROLLERS];

ControllerSelectionNode::ControllerSelectionNode(System *sys, Controller ***ctrl, const char *n) : SelectionNode(n) {

    system = sys;
    menuController = ctrl;

    for(unsigned char i = 0; i < SYSTEM_NB_CONTROLLERS; ++i){
        indexes[i] = i;
        this->addChildren(&indexes[i],system->controllers[i]->getName());
    }
}
