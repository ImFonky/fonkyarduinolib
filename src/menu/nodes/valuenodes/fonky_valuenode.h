#ifndef FONKY_FONKY_VALUENODE_H
#define FONKY_FONKY_VALUENODE_H

/*!
 * @file fonky_valuenode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class ValueNode
 * @brief This class is a element of the SelectionNode.
 * Shall not display anything because never entered.
 * This class may just contain templated value and call spécific clic handling functions
 */

#include "../abstract/fonky_node.h"

template<class T>
class ValueNode : public Node {
protected:
    //! The node data element
    const T value;
    //! The displayed node value
    const char *strValue;

public:
    /*! ValueNode constructor
     * @param name the name of this node
     * @param value the data of this node
     * @param strValue data value to be displayed
     */
    ValueNode(const char *name, T value, const char *strValue);

    T getValue();
    const char *getStrValue();

#ifdef NO_ARDUINO
    ~ValueNode();
#endif
};


#endif //FONKY_FONKY_VALUENODE_H
