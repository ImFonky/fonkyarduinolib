#include "fonky_speedselectionnode.h"
#include "../../../controllers/abstract/fonky_controller_template.h"

Node *SpeedSelectionNode::onClickSlide(const Point *p, Node *currentNode, Button *b) {

    SpeedSelectionNode* node = ((SpeedSelectionNode*)currentNode);
    char direction = (char)((b->childIndex & 0x1) == 0 ? 1 : -1);

    if(b->childIndex & 0x2){
        short newStep = (node->step + direction);
        newStep = (short)(newStep < SLIDERNODE_MAX_VALUE ? newStep : SLIDERNODE_MAX_VALUE);
        newStep = (short)(newStep > SLIDERNODE_MIN_VALUE ? newStep : SLIDERNODE_MIN_VALUE);
        node->step = (unsigned char)newStep;
    }
    else{
        short newVal = ((*node->valuesPtr) + (node->step * direction));
        newVal = (short)(newVal < SLIDERNODE_MAX_VALUE ? newVal : SLIDERNODE_MAX_VALUE);
        newVal = (short)(newVal > SLIDERNODE_MIN_VALUE ? newVal : SLIDERNODE_MIN_VALUE);
        setValue(node,newVal);
    }
    return currentNode;
}

Node *SpeedSelectionNode::onClickStep(const Point *p, Node *currentNode, Button *b) {

    SpeedSelectionNode* node = ((SpeedSelectionNode*)currentNode);

    char direction = (char)(b->childIndex == 0 ? 1 : -1);
    short newStep = (node->step + direction);
    newStep = (short)(newStep < SLIDERNODE_MAX_VALUE ? newStep : SLIDERNODE_MAX_VALUE);
    newStep = (short)(newStep > SLIDERNODE_MIN_VALUE ? newStep : SLIDERNODE_MIN_VALUE);

    node->step = (unsigned char)newStep;
    return currentNode;
}

#define MIN_SPEED 0
#define MAX_SPEED 100
#define UCHAR_IN_CHAR_ARRAY(array,uchar)\
    array[0] = (char)('0' + uchar/100);\
    array[1] = (char)('0' + (uchar%100)/10);\
    array[2] = (char)('0' + (uchar%10))

void SpeedSelectionNode::_display(SpeedSelectionNode* valueNode) {
    char strValue[12] = {'S','p','e','e','d',' ',':',' ','0','0','0',0};
    UCHAR_IN_CHAR_ARRAY((strValue+8),valueNode->getValue());
    Tft.drawString(strValue,
                   centerXtext(strValue, 0, TFT_SCREEN_WIDTH, 2),
                   centerYtext(SLIDERNODE_Y_MIN_VALUE, NODE_BUTTON_SIZE, 2),
                   2, CYAN);

    char strStep[11] = {'S','t','e','p',' ',':',' ',' ',' ',' ',0};
    UCHAR_IN_CHAR_ARRAY((strStep+7),valueNode->step);
    Tft.drawString(strStep,
                   centerXtext(strStep, 0, TFT_SCREEN_WIDTH, 2),
                   centerYtext(SLIDERNODE_Y_MIN_STEP, SLIDERNODE_KEY_HEIGHT, 2),
                   2, CYAN);

    char strBounds[16] = {'F','r','o','m',' ','0','0','0',' ','t','o',' ','0','0','0',0};
    UCHAR_IN_CHAR_ARRAY((strBounds+5),MIN_SPEED);
    UCHAR_IN_CHAR_ARRAY((strBounds+12),MAX_SPEED);
    Tft.drawString(strBounds,
                   centerXtext(strBounds, 0, TFT_SCREEN_WIDTH, 1),
                   centerYtext(SLIDERNODE_Y_MIN_HINT, SLIDERNODE_KEY_HEIGHT, 1),
                   1, CYAN);

}

void SpeedSelectionNode::display() {
    _display(this);
}

SpeedSelectionNode::SpeedSelectionNode(Controller *ctrl, Controller::Function fnt, const char *name)
        :TyperNode<unsigned char>(&value, 1, name){

    controller = ctrl;
    function = fnt;

    getValue();
    step = 10;


    makeButton(this, "SLIDE+", SLIDERNODE_X_MIN_CONTROLS, SLIDERNODE_Y_MIN_CONTROLS, &onClickSlide, &defaultCondition, 0, SLIDERNODE_KEY_WIDTH, SLIDERNODE_KEY_HEIGHT,BLACK,GREEN,1);
    makeButton(this, "SLIDE-", 0,SLIDERNODE_Y_MIN_CONTROLS, &onClickSlide, &defaultCondition, 1, SLIDERNODE_KEY_WIDTH, SLIDERNODE_KEY_HEIGHT,BLACK,GREEN,1);
    makeButton(this, "STEP+", SLIDERNODE_X_MIN_CONTROLS - SLIDERNODE_KEY_WIDTH, SLIDERNODE_Y_MIN_CONTROLS, &onClickStep, &defaultCondition, 0, SLIDERNODE_KEY_WIDTH, SLIDERNODE_KEY_HEIGHT,BLACK,GREEN,1);
    makeButton(this, "STEP-", 0 + SLIDERNODE_KEY_WIDTH,SLIDERNODE_Y_MIN_CONTROLS, &onClickStep, &defaultCondition, 1, SLIDERNODE_KEY_WIDTH, SLIDERNODE_KEY_HEIGHT,BLACK,GREEN,1);
}

unsigned char  SpeedSelectionNode::getValue(){
    value = (unsigned char) (controller->getSpeedPower(function) * 100.0);
    return value;
}

void SpeedSelectionNode::setValue(SpeedSelectionNode *node, unsigned char val){
    node->value = val;
    if (node->function == Camera::ALLFUNCT) {
        for (int i = 0; i < CAMERA_NB_FUNCTIONS; ++i) {
            node->controller->setSpeedPower((Camera::Function) i, (double) val / 100.0);
        }
    } else {
        node->controller->setSpeedPower(node->function, (double) val / 100.0);
    }
}
