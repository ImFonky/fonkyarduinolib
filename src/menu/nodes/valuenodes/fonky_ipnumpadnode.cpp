#include "fonky_ipnumpadnode.h"


Node *IpNumpadNode::onClickKey(const Point *p, Node *currentNode, Button *b) {

    // SERIAL_PRINTLN("Key clicker");

    IpNumpadNode* node = (IpNumpadNode*)currentNode;
    int testSize = node->valuesPtr[node->byteSelector] * 10;
    testSize = testSize > 255 ? testSize % 200 : testSize;
    testSize += b->childIndex;
    testSize = testSize > 255 ? 0 : testSize;
    node->valuesPtr[node->byteSelector] = testSize;

    return currentNode;
}

Node *IpNumpadNode::onClickNext(const Point *p, Node *currentNode, Button *b) {

    IpNumpadNode* node = ((IpNumpadNode*)currentNode);
    char direction = b->childIndex == 0 ? 1 : -1;
    node->byteSelector = (unsigned char)((node->byteSelector + direction) % node->valueLength);
    return currentNode;
}


void IpNumpadNode::_display(IpNumpadNode* ipNode) {
    char strIpAddress[16] = {'0','0','0','.','0','0','0','.','0','0','0','.','0','0','0',0};
    strIpAddress[0] = (char)('0' + ipNode->valuesPtr[0]/100);
    strIpAddress[1] = (char)('0' + (ipNode->valuesPtr[0]%100)/10);
    strIpAddress[2] = (char)('0' + (ipNode->valuesPtr[0]%10));
    strIpAddress[4] = (char)('0' + ipNode->valuesPtr[1]/100);
    strIpAddress[5] = (char)('0' + (ipNode->valuesPtr[1]%100)/10);
    strIpAddress[6] = (char)('0' + (ipNode->valuesPtr[1]%10));
    strIpAddress[8] = (char)('0' + ipNode->valuesPtr[2]/100);
    strIpAddress[9] = (char)('0' + (ipNode->valuesPtr[2]%100)/10);
    strIpAddress[10] = (char)('0' + (ipNode->valuesPtr[2]%10));
    strIpAddress[12] = (char)('0' + ipNode->valuesPtr[3]/100);
    strIpAddress[13] = (char)('0' + (ipNode->valuesPtr[3]%100)/10);
    strIpAddress[14] = (char)('0' + (ipNode->valuesPtr[3]%10));

    Tft.drawString(strIpAddress,
                   centerXtext(strIpAddress, 0, TFT_SCREEN_WIDTH, 2),
                   centerYtext(IPNODE_Y_MIN_VALUE, NODE_BUTTON_SIZE, 2),
                   2, CYAN);

    char focusedByte[16] = {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0};
    focusedByte[0 + ipNode->byteSelector * 4] = '^';
    focusedByte[1 + ipNode->byteSelector * 4] = '^';
    focusedByte[2 + ipNode->byteSelector * 4] = '^';
    Tft.drawString(focusedByte,
                   centerXtext(focusedByte, 0, TFT_SCREEN_WIDTH, 2),
                   centerYtext(IPNODE_Y_MIN_HINT, NUMNODE_KEY_HEIGHT, 2),
                   2, CYAN);

}

void IpNumpadNode::display() {
    _display(this);
}

IpNumpadNode::IpNumpadNode(const char *name):NumpadNode(ipAddress, 4, &onClickKey, name){
    byteSelector = 0;

    makeButton(this, "NEXT", NUMNODE_X_MIN_CONTROLS, NUMNODE_Y_MIN_CONTROLS, &onClickNext, &defaultCondition, 0, NUMNODE_KEY_WIDTH, NUMNODE_KEY_HEIGHT);
    makeButton(this, "LAST", 0,NUMNODE_Y_MIN_CONTROLS, &onClickNext, &defaultCondition, 1, NUMNODE_KEY_WIDTH, NUMNODE_KEY_HEIGHT);
}