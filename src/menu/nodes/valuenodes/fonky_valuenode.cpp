#include "fonky_valuenode.h"

template<class T>
ValueNode<T>::ValueNode(const char *name, T value, const char *strValue):value(value),strValue(strValue) {
    initNode(name);
}

template<class T>
T ValueNode<T>::getValue() {
    return value;
}

template<class T>
const char *ValueNode<T>::getStrValue() {
    return strValue;
}
