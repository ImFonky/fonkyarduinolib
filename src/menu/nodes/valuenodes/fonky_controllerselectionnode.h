#ifndef FONKY_FONKY_CONTROLLERSELECTIONNODE_H
#define FONKY_FONKY_CONTROLLERSELECTIONNODE_H

/*!
 * @file fonky_controllerselectionnode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class ControllerSelectionNode
 * @brief This class is specifically designed to bind a controller to a camera
 */

#include "../../../fonky_system.h"
#include "../menunodes/fonky_selectionnode.h"

class System;
class Controller;

class ControllerSelectionNode: public SelectionNode {
public:
    //! Manage controller selection
    static const ButtonFunction onSelectController;
protected:
    //! A pointer to the system
    System *system;
    //! An (evil) triple pointer to allow modifying bindings deep in the system
    Controller ***menuController;
    //! To keep a track of children butttons indexes (and avoid generic buttons like BACK or UP/DOWN)
    static unsigned char indexes[SYSTEM_NB_CONTROLLERS];

public:
    /*! ControllerSelectionNode constructor
     * @param system a pointer to the system
     * @param menuController a pointer to the controllers container
     * @param n the name of the node
     */
    ControllerSelectionNode(System *system, Controller ***menuController, const char *n);
};


#endif //FONKY_FONKY_CONTROLLERSELECTIONNODE_H
