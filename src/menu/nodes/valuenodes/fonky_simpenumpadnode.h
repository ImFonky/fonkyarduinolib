#ifndef FONKY_SIMPENUMPADNODE_H
#define FONKY_SIMPENUMPADNODE_H

/*!
 * @file fonky_simplenumpadnode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class SimpleNumpadNode
 * @brief This class implements simple numeric keyboard GUI
 */


#include "../abstract/fonky_numpadnode.h"

#define SIMPLENODE_Y_MIN_VALUE NODE_BUTTON_SIZE


class SimpleNumpadNode : public NumpadNode {
protected:
    //! Numpad click handler
    static const ButtonFunction onClickKey;

    //! Displays the value
    static void _display(SimpleNumpadNode *ipNode);

    //! Calls _display. @see IpNumpadNode#_display
    void display();

public:
    /*!
     * SimpleNumpadNode constructor
     * @param value is the value to be modified
     * @param name is the name of the node
     */
    SimpleNumpadNode(unsigned char *value, const char *name = "Menu");

};


#endif //FONKY_SIMPENUMPADNODE_H
