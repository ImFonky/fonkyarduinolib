#include "fonky_controllernode.h"
#include "limits.h"

Node *ControllerNode::controllerOnClickButton(const Point *p, Node *currentNode, Button *b) {
    controller->start();
    return MenuNode::onClickChildren(p, currentNode, b);
}

Node *ControllerNode::controllerOnClickBackButton(const Point *p, Node *currentNode, Button *b) {
    controller->stop();
    return defaultOnClickBackButton(p,currentNode,b);
}

void ControllerNode::setSpeed(Controller::Function function, bool minus) {
    controller->setSpeed(function, (minus ? -1 : 1) * controller->getSpeedPower(function));
}

void ControllerNode::setPosition(Controller::Function function, bool minus) {
    long normalizedDiff = (minus ? -1 : 1) * (controller->getSpeedPower(function) / 5) * LONG_MAX;
    long currentPosition = controller->getPosition(function);
    long newPosition = currentPosition + normalizedDiff;
    if (!minus && (newPosition < currentPosition)) {
        newPosition = LONG_MAX;
    } else if (minus && (newPosition > currentPosition)) {
        newPosition = LONG_MIN;
    }
    controller->setPosition(function, newPosition);
}

Node *ControllerNode::onPlusSpeed(const Point *p, Node *currentNode, Button *b) {
    setSpeed((Controller::Function) b->childIndex, false);
    return nullptr;
}

Node *ControllerNode::onMinusSpeed(const Point *p, Node *currentNode, Button *b) {
    setSpeed((Controller::Function) b->childIndex, true);
    return nullptr;
}

Node *ControllerNode::onPlusPosition(const Point *p, Node *currentNode, Button *b) {
    setPosition((Controller::Function) b->childIndex, false);
    return nullptr;
}

Node *ControllerNode::onMinusPosition(const Point *p, Node *currentNode, Button *b) {
    setPosition((Controller::Function) b->childIndex, true);
    return nullptr;
}



Controller *ControllerNode::controller;

void ControllerNode::display() {
}

void ControllerNode::releaseClick() {
    controller->setSpeed(Controller::PAN, 0.0);
    controller->setSpeed(Controller::TILT, 0.0);
    controller->setSpeed(Controller::ZOOM, 0.0);
}

Node  *ControllerNode::checkClick(const Point *p, Node *currentNode) {
    return nullptr;
}

ControllerNode::ControllerNode(Controller *ptz, const char *name) {
    initNode(name);

    controller = ptz;

    //left right buttons (PAN)
    makeButton(this, "PAN-",
               0, NODE_BUTTON_SIZE * 3,
               &onPlusSpeed, &defaultCondition, Controller::PAN);

    makeButton(this, "PAN+",
               NODE_BUTTON_SIZE * 2, NODE_BUTTON_SIZE * 3,
               &onMinusSpeed, &defaultCondition, Controller::PAN);

    // Up down buttons (TILT)
    makeButton(this, "TILT+",
               NODE_BUTTON_SIZE, NODE_BUTTON_SIZE * 2,
               &onPlusSpeed, &defaultCondition, Controller::TILT);

    makeButton(this, "TILT-",
               NODE_BUTTON_SIZE, NODE_BUTTON_SIZE * 4,
               &onMinusSpeed, &defaultCondition, Controller::TILT);

    // Zoom buttons
    makeButton(this, "ZOOM+",
               NODE_BUTTON_SIZE*2, NODE_BUTTON_SIZE,
               &onPlusSpeed, &defaultCondition, Controller::ZOOM, NODE_BUTTON_SIZE, NODE_BUTTON_SIZE, BLACK, RED);

    makeButton(this, "ZOOM-",
               0, NODE_BUTTON_SIZE,
               &onMinusSpeed, &defaultCondition, Controller::ZOOM, NODE_BUTTON_SIZE, NODE_BUTTON_SIZE, BLACK, RED);

    // Focus buttons
    makeButton(this, "FOC+",
               NODE_BUTTON_SIZE*2, NODE_BUTTON_SIZE*2,
               &onPlusPosition, &defaultCondition, Controller::FOCUS, NODE_BUTTON_SIZE, NODE_BUTTON_SIZE, BLACK,
               YELLOW);

    makeButton(this, "FOC-",
               0, NODE_BUTTON_SIZE*2,
               &onMinusPosition, &defaultCondition, Controller::FOCUS, NODE_BUTTON_SIZE, NODE_BUTTON_SIZE, BLACK,
               YELLOW);

    // Iris buttons
    makeButton(this, "IRIS+",
               NODE_BUTTON_SIZE*2, NODE_BUTTON_SIZE*4,
               &onPlusPosition, &defaultCondition, Controller::IRIS, NODE_BUTTON_SIZE, NODE_BUTTON_SIZE, BLACK, YELLOW);

    makeButton(this, "IRIS-",
               0, NODE_BUTTON_SIZE*4,
               &onMinusPosition, &defaultCondition, Controller::IRIS, NODE_BUTTON_SIZE, NODE_BUTTON_SIZE, BLACK,
               YELLOW);

}