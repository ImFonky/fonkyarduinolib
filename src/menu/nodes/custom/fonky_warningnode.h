#ifndef FONKY_WARNINGNODE_H
#define FONKY_WARNINGNODE_H

/*!
 * @file fonky_warningnode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class WarningNode
 * @brief Implements a pseudo dialog windows that allow decision verification over GUI.
 */


#include "../abstract/fonky_node.h"

#define WARNINGNODE_BUTTON_WIDTH TFT_SCREEN_WIDTH
#define WARNINGNODE_BUTTON_HEIGHT NODE_BUTTON_SIZE
#define WARNINGNODE_BUTTON_Y (TFT_SCREEN_HEIGHT - WARNINGNODE_BUTTON_HEIGHT)

#define WARNINGNODE_TEXT_NB_LINES 3
#define WARNINGNODE_TEXT_WIDTH WARNINGNODE_BUTTON_WIDTH
#define WARNINGNODE_TEXT_HEIGHT (WARNINGNODE_BUTTON_HEIGHT / 2)
#define WARNINGNODE_TEXT_Y (2 * NODE_BUTTON_SIZE)

class WarningNode : public Node {
protected:

    //! The warning message to be displayed
    const char *warningMessage[WARNINGNODE_TEXT_NB_LINES];

    //! Displays the warning message
    void display();

    //! Implented to make the class no more abstract. @see Node#checkClick
    Node *checkClick(const Point *p, Node *currentNode);

public:
    /*! WarningNode constructor
     * @param name the name of the Node
     * @param text the text of the warning message
     * @param action the action to execute if the user press the ok button
     */
    WarningNode(const char *name, const char *text[WARNINGNODE_TEXT_NB_LINES], ButtonFunctionPtr action);
};


#endif //FONKY_WARNINGNODE_H
