#ifndef FONKY_CONTROLLERNODE_H
#define FONKY_CONTROLLERNODE_H

/*!
 * @file fonky_controllernode.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class ControllerNode
 * @brief Implements a pseudo dialog windows that allow decision verification over GUI.
 */


#include "../abstract/fonky_node.h"
#include "../../../cameras/abstract/fonky_camera_template.h"
#include "../menunodes/fonky_menunode.h"
#include "../../../controllers/abstract/fonky_controller_template.h"

class ControllerNode : public Node {
public :
    //! The "entering node" event handling function
    static const ButtonFunction controllerOnClickButton;
    //! The "leaving node" event handling function
    static const ButtonFunction controllerOnClickBackButton;
protected :
    static void setSpeed(Controller::Function function, bool minus);
    static void setPosition(Controller::Function function, bool minus);

    //! Handles speeds incrementation
    static const ButtonFunction onPlusSpeed;
    //! Handles speeds decrementation
    static const ButtonFunction onMinusSpeed;
    //! Handles positions incrementation
    static const ButtonFunction onPlusPosition;
    //! Handles positions decrementation
    static const ButtonFunction onMinusPosition;

    //! The system scheduler
    static Controller *controller;

    //! Implented to make the class no more abstract. @see Node#display
    void display();

    //! Implented to make the class no more abstract. @see Node#checkClick
    Node *checkClick(const Point *p, Node *currentNode);

public:
    /*! WarningNode constructor
     * @param ptzCamera the currently controlled camera
     * @param name the name of the Node
     */
    ControllerNode(Controller *ptzCamera, const char *name = "Menu");

    //! Overrides mother method that does nothing
    void releaseClick();

#ifdef NO_ARDUINO
    ~ControllerNode();
#endif
};


#endif //FONKY_CONTROLLERNODE_H
