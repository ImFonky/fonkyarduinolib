//
// Created by pbmon on 24.07.2018.
//

#include "fonky_warningnode.h"


void WarningNode::display() {
    for (unsigned char i = 0; i < WARNINGNODE_TEXT_NB_LINES; ++i) {
        Tft.drawString(warningMessage[i],
                       centerXtext(warningMessage[i], 0, WARNINGNODE_TEXT_WIDTH, 1),
                       centerYtext(WARNINGNODE_TEXT_Y + (WARNINGNODE_TEXT_HEIGHT * i), WARNINGNODE_TEXT_HEIGHT, 1),
                       1, RED);
    }
}

Node *WarningNode::checkClick(const Point *p, Node *currentNode) {
    return nullptr;
}

WarningNode::WarningNode(const char *name, const char *text[WARNINGNODE_TEXT_NB_LINES], ButtonFunctionPtr action) {
    initNode(name);

    for (unsigned char i = 0; i < WARNINGNODE_TEXT_NB_LINES; ++i) {
        warningMessage[i] = text[i];
    }

    makeButton(this, "OK",
               0, WARNINGNODE_BUTTON_Y,
               action, &defaultCondition, 0,
               WARNINGNODE_BUTTON_WIDTH, WARNINGNODE_BUTTON_HEIGHT,
               BLACK, RED, 3);

}
