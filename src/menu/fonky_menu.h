#ifndef FONKYMENU_H
#define FONKYMENU_H

/*!
 * @file fonky_menu.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section MENU
 * @class Menu
 * @brief This class is the GUI and touch screen manager.
 * In order to display dynamical GUI, the menu work with nodes.
 * Nodes are similar to web pages or windows. They have a content and they change when pressing on buttons.
 */

#include "../utils/fonky_seeed_touch_screen.h"
#include <TFTv2.h>

#include "../cameras/heads/kesslercrane_revhead.h"
#include "nodes/clusters/fonky_rootmenunode.h"
#include "../fonky_system.h"

#define MAX_TOUCH_TICKS 10

#include <Thread.h>
#include <ThreadController.h>

class RootMenuNode;

class System;

class Menu {
private:
    //! The device touch screen
    static TouchScreen *screen;
    //! This pointer to the system allows to share modules with the GUI
    static System *system;

    //! This pointer is the first node of the menu. It contains main paths to device functions management
    static RootMenuNode *rootNode;
    //! This pointer is the current displayed node
    static Node *currentNode;
    //! Avoids screen to be displayed when no change occurred
    static bool displayed;

    //! Thread that handles click detection and GUI display
    static Thread menuThread;
    //! Number of tics before a new touch event after changing node
    static unsigned short touchTicks;

    //! Initializes screen and touch functions
    void setup_screen();

    //! Prepares menu and set rootNode as first node
    void setup_menu();

    //! Manages GUI display
    static void DisplayMenu();

    //! Manages touch detection
    static void CheckTouch();

    //! Manages touch detection
    void launchTasks();

public:
    //! Menu default constructor
    Menu();

    /*! Menu constructor
     * @param system a pointer to the system
     */
    Menu(System *system);


#ifdef NO_ARDUINO
    ~Menu();
#endif
};

#endif