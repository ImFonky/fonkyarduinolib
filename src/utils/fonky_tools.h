#ifndef FONKY_TOOLS_H
#define FONKY_TOOLS_H

/*!
 * @file fonky_tools.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section UTILS
 * @brief This file contain all purpose tools functions made during the project.
 */

/*! Simple string comparator
  @param str1 a string to compare
  @param str2 a string to compare
  @return Returns the position of the last matching character.
*/
char fonky_strcmp(const char *str1, const char *str2);

#endif //FONKY_TOOLS_H
