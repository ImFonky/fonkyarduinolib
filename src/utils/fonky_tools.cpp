//
// Created by pbmon on 18.07.2018.
//

#include "fonky_utils.h"

char fonky_strcmp(const char *str1, const char *str2) {
    if (str1 == nullptr || str2 == nullptr)
        return 0;

    char i = 0;
    while (str1[i] && str2[i]) {
        if (str1[i] != str2[i])
            return -i;
        ++i;
    }

    return i;
}

