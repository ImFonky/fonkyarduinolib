
#ifndef FONKY_NOREDEFSEEEDTOUCHSCREEN_H
#define FONKY_NOREDEFSEEEDTOUCHSCREEN_H

/*!
 * @file fonky_seeed_touch_screen.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section UTILS
 * @brief This file wraps SeeedTouchScreen.h library in define tags to avoid error when included multiple times.
 */


#include <SeeedTouchScreen.h>

#endif //FONKY_NOREDEFSEEEDTOUCHSCREEN_H
