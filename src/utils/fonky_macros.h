#ifndef FONKY_FONKY_MACROS_H
#define FONKY_FONKY_MACROS_H

/*!
 * @file fonky_macros.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section UTILS
 * @brief This file contains debugging macros that can be enabled or disabled to easily manage dev or prod scenarios.
 */

#include "fonky_globals.h"

#define SYMBOL_TO_STRING(symbol) #symbol

#if SERIAL_DEBUG
#define SERIAL_PRINT(value) Serial.print(value)
#define SERIAL_PRINTLN(value) Serial.println(value)
#else
#define SERIAL_PRINT(value)
#define SERIAL_PRINTLN(value)
#endif

#if MEMORY_TEST
#define DO_MEMORY_TEST()\
        Serial.flush();\
        Serial.print(F(" | Free memory : "));\
        Serial.println(freeMemory())
#else
#define DO_MEMORY_TEST()
#endif

#if SPEED_TEST
#define DO_SPEED_TEST(name, wait) \
    static unsigned long name##count = 0;\
    static unsigned long name##diff = 0;\
    static unsigned long name##timeElapsed = 0;\
    if( !(name ## count % wait)) {\
        if(name ## timeElapsed == 0){\
            name ## timeElapsed = millis();\
        }\
        name ## diff = (millis() - (name ## timeElapsed));\
        Serial.flush();\
        Serial.print(#name " --> Elapsed : ");\
        Serial.print(name ## diff);\
        Serial.print(" | Nb iterations : ");\
        Serial.print(name ## count);\
        Serial.print(" | it/ms : ");\
        Serial.println((float)(name ## count) / (name ## diff));\
        DO_MEMORY_TEST();\
    }\
    name ## count++
#else
#define DO_SPEED_TEST(name,wait)
#endif

#endif //FONKY_FONKY_MACROS_H
