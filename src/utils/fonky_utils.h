//
// Created by pbmon on 18.07.2018.
//

#ifndef FONKY_FONKY_UTILS_H
#define FONKY_FONKY_UTILS_H

/*!
 * @file fonky_utils.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section UTILS
 * @brief This files wrap the 3 utility files in one for easier imports.
 */

#include "fonky_globals.h"
#include "fonky_macros.h"
#include "fonky_tools.h"

#endif //FONKY_FONKY_UTILS_H
