#ifndef FONKY_GLOBALS_H
#define FONKY_GLOBALS_H

/*!
 * @file fonky_globals.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section UTILS
 * @brief This file contains global definitions for system and debug configuration.
 */

#define STATIC_ALLOC 0
#define SERIAL_DEBUG 0
#define SPEED_TEST 0
#define MEMORY_TEST 1

#if MEMORY_TEST
#include <MemoryFree.h>
#endif

#endif //FONKY_GLOBALS_H
