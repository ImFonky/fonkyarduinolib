#include "fonky_network.h"

const void Network::DefaultNetworkHandler(EthernetClient *client) {
    SERIAL_PRINTLN("DefaultNetworkHandler");
}

const unsigned char Network::DEFAULT_DEVICE_MAC_ADDRESS[6] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
const unsigned char Network::DEFAULT_DEVICE_IP[4] = {192, 168, 1, 69};
const unsigned char Network::DEFAULT_DEVICE_PORT = 80;

unsigned char Network::devicePort;
unsigned char Network::deviceMac[6];
IPAddress Network::deviceIp;
EthernetServer *Network::server;
EthernetClient Network::srvClient;
EthernetClient *Network::client;

ThreadController *Network::masterThread;
Thread Network::clientThread;
Thread Network::serverThread;

Network::NetworkJobPtr Network::ParseControllerURL;
Network::NetworkJobPtr Network::CameraJobs[NETWORK_MAX_CAMERA_JOBS];
unsigned char Network::nbCameraJobs;


void Network::ListenRequest() {
    DO_SPEED_TEST(ListenRequest, 1000);
    if (server != nullptr) {
        srvClient = server->available();
        if (srvClient) {
            SERIAL_PRINTLN(F("new client"));
            ParseControllerURL(&srvClient);
            SERIAL_PRINTLN(F("client disconnected"));
        }
    }
}

void Network::SendRequest() {
    if (client != nullptr) {
        for (unsigned char i = 0; i < nbCameraJobs; ++i) {
            CameraJobs[i](client);
        }
    }
}

void Network::launchTasks() {
    serverThread.onRun(ListenRequest);
    serverThread.setInterval(25);
    clientThread.onRun(SendRequest);
    clientThread.setInterval(100);
    masterThread->add(&serverThread);
    masterThread->add(&clientThread);
}

Network::Network() {
    setDeviceMac((unsigned char *) DEFAULT_DEVICE_MAC_ADDRESS);
    setDeviceIp((unsigned char *) DEFAULT_DEVICE_IP);
    setDevicePort(DEFAULT_DEVICE_PORT);
    nbCameraJobs = 0;
    ParseControllerURL = (NetworkJobPtr) &DefaultNetworkHandler;

}

Network::Network(ThreadController *master):Network(){
    masterThread = master;
}

Network::Network(ThreadController *master, unsigned char macAddress[6], unsigned char ipAddress[4],
                 unsigned char tcpPort) {
    masterThread = master;
    setDeviceMac(macAddress);
    setDeviceIp(ipAddress);
    setDevicePort(tcpPort);
}

void Network::start(){
    SERIAL_PRINTLN(F("Starting network"));

//    Ethernet.init();

    if (deviceIp == IPAddress(0, 0, 0, 0)) {
        SERIAL_PRINTLN(F("Device adress : 0.0.0.0"));
        SERIAL_PRINTLN(F("Looking for DHCP..."));
        Ethernet.begin(deviceMac);
        if (Ethernet.localIP() == IPAddress(0, 0, 0, 0)) {
            SERIAL_PRINTLN(F("No DHCP found, network can't start"));
            return;
        }
    } else {
        Ethernet.begin(deviceMac, deviceIp);
    }

    // give the Ethernet shield a second to initialize:
    delay(1000);

    server = new EthernetServer(devicePort);

    server->begin();

    client = new EthernetClient();
    SERIAL_PRINT(F("server is at "));
    SERIAL_PRINTLN(Ethernet.localIP());
    SERIAL_PRINTLN(F("Network started"));
    Serial.flush();

    launchTasks();
}

void Network::stop() {
    delete client;
    delete server;
}

void Network::reset() {
    stop();
    start();
}

Network::MacAddress Network::getDeviceMac() {
    return {
            deviceMac[0],
            deviceMac[1],
            deviceMac[2],
            deviceMac[3],
            deviceMac[4],
            deviceMac[5]
    };
}

IPAddress Network::getDeviceIp() {
    return deviceIp;
}

unsigned char Network::getDevicePort() {
    return devicePort;
}

void Network::setDeviceMac(unsigned char macAddress[6]) {
    deviceMac[0] = macAddress[0];
    deviceMac[1] = macAddress[1];
    deviceMac[2] = macAddress[2];
    deviceMac[3] = macAddress[3];
    deviceMac[4] = macAddress[4];
    deviceMac[5] = macAddress[5];
}

void Network::setDeviceIp(unsigned char ipAddress[4]){
    deviceIp = IPAddress(ipAddress);
}

void Network::setDevicePort(unsigned char portNumber) {
    devicePort = portNumber;
}

void Network::setControllerJob(NetworkJobPtr controllerJob) {
    ParseControllerURL = controllerJob;
}

void Network::addCameraJob(NetworkJobPtr cameraJob) {
    if (nbCameraJobs < NETWORK_MAX_CAMERA_JOBS) {
        CameraJobs[nbCameraJobs] = cameraJob;
        nbCameraJobs++;
    } else {
        SERIAL_PRINTLN("No more space for camera job");
    }
}
