#ifndef FONKYNETWORK_H
#define FONKYNETWORK_H

/*!
 * @file fonky_network.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section NETWORK
 * @class Network
 * @brief This class is responsible to manage the Ethernet Shield and provide both client and server abilities for actors of the system.
 * @see  http://wiki.seeedstudio.com/Ethernet_Shield_V2.0/
 */


#include "Arduino.h"
#include <SPI.h>
#include <Ethernet2.h>

#include <Thread.h>
#include <ThreadController.h>


#include "../utils/fonky_utils.h"

#define NETWORK_MAX_CAMERA_JOBS 5
#define W5500_CS 10


class Network {
public:

    //! Defines a special types of function to handle a client.
    typedef void (NetworkJob)(EthernetClient *client);

    //! Defines a pointer to NetworkJob
    typedef NetworkJob *NetworkJobPtr;

    //! This default NetworkJob does nothing
    static const void DefaultNetworkHandler(EthernetClient *client);

    //! Device default MAC address
    static const unsigned char DEFAULT_DEVICE_MAC_ADDRESS[6];
    //! Device default IP address
    static const unsigned char DEFAULT_DEVICE_IP[4];
    //! Server default port
    static const unsigned char DEFAULT_DEVICE_PORT;

    //! Defines a MAC address structure
    typedef struct st_MacAddress {
        unsigned char a;
        unsigned char b;
        unsigned char c;
        unsigned char d;
        unsigned char e;
        unsigned char f;
    } MacAddress;

protected:
    //! Server current TCP port
    static unsigned char devicePort;
    //! Device current MAC address
    static unsigned char deviceMac[6];
    //! Device current IP address
    static IPAddress deviceIp;

    //! The server that will handle client's TCP requests
    static EthernetServer *server;
    //! The client that will be handled by a controller object
    static EthernetClient srvClient;
    //! The client that will be handled by multiple camera objects
    static EthernetClient *client;

    //! The system scheduler
    static ThreadController *masterThread;
    //! The client or camera thread
    static Thread clientThread;
    //! The server or controller thread
    static Thread serverThread;

    //! The job to be done by the controller object @see Network#setControllerJob
    static NetworkJobPtr ParseControllerURL;
    //! The job to be done by camera objects @see Network#addCameraJob
    static NetworkJobPtr CameraJobs[NETWORK_MAX_CAMERA_JOBS];
    //! The number of camera objects using the Network class as TCP client
    static unsigned char nbCameraJobs;

    //! Calls the controller NetworkJob method
    static void ListenRequest();

    //! Calls cameras NetworkJob methods
    static void SendRequest();

    //! Sets up controller and cameras threads
    static void launchTasks();


public:
    //! Network default constructor
    Network();

    /*! Network constructor with default network settings
     * @param master a pointer to the system scheduler
     */
    Network(ThreadController *master);

    /*! Network constructor with network settings
     * @param master a pointer to the system scheduler
     * @param macAddress the ethernet interface MAC address
     * @param ipAddress the ethernet interface IP address
     * @param tcpPort the server TCP port
     */
    Network(ThreadController *master, unsigned char macAddress[6], unsigned char ipAddress[4], unsigned char tcpPort);

    //! Starting the threads
    static void start();

    //! Stopping the threads
    static void stop();

    //! Resetting network system
    static void reset();

    static MacAddress getDeviceMac();
    static IPAddress getDeviceIp();
    static unsigned char getDevicePort();

    static void setDeviceMac(unsigned char macAddress[6]);
    static void setDeviceIp(unsigned char ipAddress[4]);
    static void setDevicePort(unsigned char portNumber);

    /*! Allows controllers to inject their own server handler
     * @param controllerJob a pointer to the controller request handler
     */
    static void setControllerJob(NetworkJobPtr controllerJob);

    /*! Allows cameras to inject their own client handler
     * @param controllerJob a pointer to the camera call handler
     */
    static void addCameraJob(NetworkJobPtr cameraJob);
};

#endif