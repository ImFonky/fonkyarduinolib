#ifndef FONKYMOTOR_H
#define FONKYMOTOR_H

/*!
 * @file kesslercrane_revhead.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section CAMERA
 * @class KesslercraneRevhead
 * @brief Rev Head driver
 * This class makes use of the DualVNH5019MotorShield library witch allows to control 2 DC motors.
 */

#include "Arduino.h"

#include "../../utils/fonky_utils.h"

#include <DualVNH5019MotorShield.h>
#include <ThreadController.h>


#define NB_MOTORS 2
#define MAX_SPEED 400
#define MIN_SPEED -MAX_SPEED
#define ZERO_SPEED 0
#define MAX_BRAKE 400
#define ZERO_BRAKE 0

class KesslercraneRevhead {

private:

    //! The system scheduler
    static ThreadController *masterThread;
    //! The motor command thread
    static Thread commandThread;

    //! The motor command shield
    static DualVNH5019MotorShield shield;

    //! The motors encoder pin A
    static unsigned char encoderPinA[NB_MOTORS];
    //! The motors encoder pin B
    static unsigned char encoderPinB[NB_MOTORS];
    //! The motors driver arduino pin
    static unsigned char driverPin[NB_MOTORS];

    static long motorPos[NB_MOTORS];
    static long motorUpperBound[NB_MOTORS];
    static long motorLowerBound[NB_MOTORS];
    static short motorSpeed[NB_MOTORS];

    //! Allows to switch PAN/TILT motor connector
    static unsigned char bindings[NB_MOTORS];

    static void setup_encoder();
    static void setup_driver();

    //! Verifies shortcut or ground
    static void stopIfFault();

    //! Simple init test
    static void init_test();

    //! Periodic task converting speed to power and driving motor
    static void ControlMotor();

#if NO_ARDUINO
    static Thread positionThread;
    static void CalculatePosition();
#endif

public:

    //! Sets up motor driving task
    static void launchTasks();

    static bool motorRunning;

    //! Defines Rev Head functions
    enum Function {
        PAN, TILT
    };

    //! KesslercraneRevhead default constructor
    KesslercraneRevhead();

    /*!
     * KesslercraneRevhead constructor
     * @param enc1A the motor 1 encoder pin A
     * @param enc1B the motor 1 encoder pin B
     * @param enc2A the motor 2 encoder pin A
     * @param enc2B the motor 2 encoder pin B
     * @param dri1 the driver 1 pin
     * @param dri2 the driver 2 pin
     * @param pos1 the motor 1 initial position
     * @param pos2 the motor 2 initial position
     * @param mot1F the connector 1 function
     * @param mot2F the connector 2 function
     */
    KesslercraneRevhead(unsigned char enc1A, unsigned char enc1B, unsigned char enc2A, unsigned char enc2B, unsigned char dri1, unsigned char dri2,
                        long pos1 = 0, long pos2 = 0,
                        Function mot1F = PAN, Function mot2F = TILT);

    /*!
     * KesslercraneRevhead constructor
     * @param master the system sheduler
     * @param enc1A the motor 1 encoder pin A
     * @param enc1B the motor 1 encoder pin B
     * @param enc2A the motor 2 encoder pin A
     * @param enc2B the motor 2 encoder pin B
     * @param dri1 the driver 1 pin
     * @param dri2 the driver 2 pin
     * @param pos1 the motor 1 initial position
     * @param pos2 the motor 2 initial position
     * @param mot1F the connector 1 function
     * @param mot2F the connector 2 function
     */
    KesslercraneRevhead(ThreadController *master, unsigned char enc1A, unsigned char enc1B, unsigned char enc2A, unsigned char enc2B, unsigned char dri1, unsigned char dri2,
                        long pos1 = 0, long pos2 = 0,
                        Function mot1F = PAN, Function mot2F = TILT);

    /*! Assign connector Pan function
     * @param motorNumber the connector number
     */
    void setPanMotor(unsigned char motorNumber);

    /*! Assign connector Tilt function
     * @param motorNumber the connector number
     */
    void setTiltMotor(unsigned char motorNumber);

    void setSpeed(Function func, double speed);
    double getSpeed(Function func);
    long getPosition(Function func);
    long getUpperBound(Function function);
    long getLowerBound(Function function);

    //! @see Camera#setUpperBound
    bool setUpperBound(Function func);

    //! @see Camera#setLowerBound
    bool setLowerBound(Function func);

    //! @see Camera#unsetUpperBound
    void unsetUpperBound(Function function);

    //! @see Camera#unsetLowerBound
    void unsetLowerBound(Function function);

    //! Starts the Rev Head
    void start();

    //! Stops the Rev Head
    void stop();

    //! Rapid stops the Rev Head
    void emergency();

    //! Resets Rev Head
    void reset();
};

#endif