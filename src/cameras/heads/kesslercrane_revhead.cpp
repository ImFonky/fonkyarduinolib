#include "kesslercrane_revhead.h"
#include "limits.h"

ThreadController *KesslercraneRevhead::masterThread;
Thread KesslercraneRevhead::commandThread;
DualVNH5019MotorShield KesslercraneRevhead::shield;

unsigned char KesslercraneRevhead::encoderPinA[NB_MOTORS];
unsigned char KesslercraneRevhead::encoderPinB[NB_MOTORS];

unsigned char KesslercraneRevhead::driverPin[NB_MOTORS];

long KesslercraneRevhead::motorPos[NB_MOTORS];

long KesslercraneRevhead::motorUpperBound[NB_MOTORS];

long KesslercraneRevhead::motorLowerBound[NB_MOTORS];

short KesslercraneRevhead::motorSpeed[NB_MOTORS];

bool KesslercraneRevhead::motorRunning;

unsigned char KesslercraneRevhead::bindings[NB_MOTORS];

#define decoding(encA, encB, encPos, lastPos)\
{\
  int n = digitalRead(encA);\
  if ((lastPos == LOW) && (n == HIGH)) {\
    if (digitalRead(encB) == LOW) {\
        encPos--;\
    } else {\
        encPos++;\
    }\
  }\
  lastPos = n;\
}
#define setMotorSpeed(motorNumber, speed) motorSpeed[motorNumber] = ((speed*MAX_SPEED))

#define getMotorSpeed(motorNumber) (motorSpeed[motorNumber]/MAX_SPEED)

#define getMotorPosition(motorNumber) motorPos[motorNumber]

void KesslercraneRevhead::setup_encoder() {
    pinMode(encoderPinA[0], INPUT);
    pinMode(encoderPinB[0], INPUT);
    pinMode(encoderPinA[1], INPUT);
    pinMode(encoderPinB[1], INPUT);
}

void KesslercraneRevhead::setup_driver() {
    pinMode(driverPin[0], INPUT_PULLUP);
    pinMode(driverPin[1], INPUT_PULLUP);
    shield.init();
}

void KesslercraneRevhead::stopIfFault() {
    if (shield.getM1Fault()) {
        SERIAL_PRINTLN(F("M1 fault"));
        motorRunning = false;
        shield.setM1Brake(MAX_BRAKE);
        shield.setM1Speed(ZERO_SPEED);
    }
    if (shield.getM2Fault()) {
        SERIAL_PRINTLN(F("M2 fault"));
        motorRunning = false;
        shield.setM2Brake(MAX_BRAKE);
        shield.setM2Speed(ZERO_SPEED);
    }
}

void KesslercraneRevhead::init_test() {
    SERIAL_PRINTLN(F("Testing shield connexion"));
    if (!digitalRead(driverPin[0])) {
        motorRunning = false;
        SERIAL_PRINTLN(F("M1 fault"));
    }
    if (!digitalRead(driverPin[1])) {
        SERIAL_PRINTLN(F("M2 fault"));
        motorRunning = false;
    }
    SERIAL_PRINTLN(F("Checking for faults"));
    stopIfFault();
    SERIAL_PRINTLN(F("Shield test done"));
}

void KesslercraneRevhead::ControlMotor(){
    if (motorRunning) {
        if (((motorSpeed[0] < 0) && (motorPos[0] > motorLowerBound[0])) ||
            ((motorSpeed[0] > 0) && (motorPos[0] < motorUpperBound[0]))) {
            shield.setM1Speed(motorSpeed[0]);
            motorPos[0] += motorSpeed[0];
        } else {
            shield.setM1Speed(ZERO_SPEED);
        }
        if (((motorSpeed[1] < 0) && (motorPos[1] > motorLowerBound[1])) ||
            ((motorSpeed[1] > 0) && (motorPos[1] < motorUpperBound[1]))) {
            shield.setM2Speed(motorSpeed[1]);
            motorPos[1] += motorSpeed[1];
        } else {
            shield.setM2Speed(ZERO_SPEED);
        }

//        shield.setSpeeds(motorSpeed[0], motorSpeed[1]);
        stopIfFault();
    } else {
        shield.setSpeeds(ZERO_SPEED,ZERO_SPEED);
    }
}

#if NO_ARDUINO
Thread KesslercraneRevhead::positionThread;

void KesslercraneRevhead::CalculatePosition(){
    long encoderLastA[2] = {LOW, LOW};
    decoding(encoderPinA[0], encoderPinB[0], motorPos[0], encoderLastA[0]);
    decoding(encoderPinA[1], encoderPinB[1], motorPos[1], encoderLastA[1]);
    DO_SPEED_TEST(decode, 10000);
}
#endif

void KesslercraneRevhead::launchTasks() {
    commandThread.onRun(ControlMotor);
    commandThread.setInterval(10);
    masterThread->add(&commandThread);
#if NO_ARDUINO
    positionThread.onRun(CalculatePosition);
    positionThread.setInterval(1);
    masterThread->add(&positionThread);
#endif
}

KesslercraneRevhead::KesslercraneRevhead(){}

KesslercraneRevhead::KesslercraneRevhead(unsigned char enc1A, unsigned char enc1B, unsigned char enc2A, unsigned char enc2B, unsigned char dri1, unsigned char dri2,
             long pos1, long pos2,
             Function mot1F, Function mot2F) {

    motorSpeed[0] = 0;
    motorSpeed[1] = 0;

    encoderPinA[0] = enc1A;
    encoderPinB[0] = enc1B;
    encoderPinA[1] = enc2A;
    encoderPinB[1] = enc2B;
    driverPin[0] = dri1;
    driverPin[1] = dri2;

    motorPos[0] = pos1;
    motorPos[1] = pos2;

    motorUpperBound[0] = LONG_MAX;
    motorUpperBound[1] = LONG_MAX;

    motorLowerBound[0] = LONG_MIN;
    motorLowerBound[1] = LONG_MIN;

    bindings[mot1F] = 0;
    bindings[mot2F] = 1;

    setup_encoder();
    setup_driver();
    init_test();

    motorRunning = false;

    SERIAL_PRINTLN(F("KesslercraneRevhead initialized"));
}

KesslercraneRevhead::KesslercraneRevhead(ThreadController *master, unsigned char enc1A, unsigned char enc1B, unsigned char enc2A, unsigned char enc2B, unsigned char dri1, unsigned char dri2,
                                         long pos1, long pos2,
                                         Function mot1F, Function mot2F):
        KesslercraneRevhead(enc1A, enc1B, enc2A, enc2B, dri1, dri2, pos1, pos2, mot1F, mot2F) {
    masterThread = master;
}

void KesslercraneRevhead::setPanMotor(unsigned char motorNumber) {
    bindings[PAN] = motorNumber;
}

void KesslercraneRevhead::setTiltMotor(unsigned char motorNumber) {
    bindings[TILT] = motorNumber;
}

void KesslercraneRevhead::setSpeed(Function func, double speed) {
    setMotorSpeed(bindings[func], speed);
}

double KesslercraneRevhead::getSpeed(Function func) {
    return getMotorSpeed(bindings[func]);
}

long KesslercraneRevhead::getPosition(Function func) {
    return getMotorPosition(bindings[func]);
}

long KesslercraneRevhead::getUpperBound(Function func) {
    return motorUpperBound[func];
}

long KesslercraneRevhead::getLowerBound(Function func) {
    return motorLowerBound[func];
}


bool KesslercraneRevhead::setUpperBound(Function func){
    if(motorSpeed[func] == 0){
        motorUpperBound[func] = motorPos[func];
        return true;
    }
    return false;
}

bool KesslercraneRevhead::setLowerBound(Function func){
    if(motorSpeed[func] == 0){
        motorLowerBound[func] = motorPos[func];
        return true;
    }
    return false;
}

void KesslercraneRevhead::unsetUpperBound(Function func) {
    motorUpperBound[func] = LONG_MAX;
}

void KesslercraneRevhead::unsetLowerBound(Function func) {
    motorLowerBound[func] = LONG_MIN;
}


void KesslercraneRevhead::start() {
    if (!motorRunning) {
        motorRunning = true;
        SERIAL_PRINTLN(F("KesslercraneRevhead started"));
    } else {
        SERIAL_PRINTLN(F("KesslercraneRevhead allready running"));
    }
    shield.setSpeeds(ZERO_SPEED, ZERO_SPEED);
}

void KesslercraneRevhead::stop() {
    motorRunning = false;
    shield.setSpeeds(ZERO_SPEED, ZERO_SPEED);
}

void KesslercraneRevhead::emergency() {
    shield.setBrakes(MAX_BRAKE, MAX_BRAKE);
    stop();
}

void KesslercraneRevhead::reset() {
    shield.setBrakes(ZERO_BRAKE, ZERO_BRAKE);
    shield.setSpeeds(ZERO_SPEED, ZERO_SPEED);
    motorRunning = true;
}


