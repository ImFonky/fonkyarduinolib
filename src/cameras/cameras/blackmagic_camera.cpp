#include "blackmagic_camera.h"
#include "limits.h"
#include "../abstract/fonky_camera_template.h"

//BMD_SDICameraControl_Serial *BlackMagicCamera::sdiCamera;
BMD_SDICameraControl_I2C *BlackMagicCamera::sdiCamera;
ThreadController *BlackMagicCamera::masterThread;
Thread BlackMagicCamera::commandThread;

bool BlackMagicCamera::everSent;
unsigned long BlackMagicCamera::lastSentTimeMs;


float BlackMagicCamera::positions[BMSDI_NB_FUNCTIONS];
float BlackMagicCamera::speeds[BMSDI_NB_FUNCTIONS];
bool BlackMagicCamera::waiters[BMSDI_NB_FUNCTIONS];
bool BlackMagicCamera::actives[BMSDI_NB_FUNCTIONS];

long BlackMagicCamera::cameraUpperBound[BMSDI_NB_FUNCTIONS];
long BlackMagicCamera::cameraLowerBound[BMSDI_NB_FUNCTIONS];

unsigned char BlackMagicCamera::cameraId;

bool BlackMagicCamera::enabled;

unsigned char BlackMagicCamera::currentOperation;


bool BlackMagicCamera::okToSend(Function function) {
    if (!actives[function] || waiters[function]) {
        return false;
    } else {
        waiters[function] = true;
        return waiters[function];
    }
}

void BlackMagicCamera::messageSent(Function function) {
    waiters[function] = false;
    actives[function] = false;
}


void BlackMagicCamera::ControlCamera() {
    unsigned long timeNow = millis();
    if (enabled && (!everSent || (timeNow - lastSentTimeMs) >= BMSDI_TIME_SLEEP)) {

        DO_SPEED_TEST(camera, 1000);

        switch (currentOperation) {
            case ZOOM:
                setZoom();
                break;
            case FOCUS:
                setFocus();
                break;
            case IRIS:
                setIris();
                break;
        }

        currentOperation = (unsigned char) ((currentOperation + 1) % BMSDI_NB_FUNCTIONS);

        everSent = true;
        lastSentTimeMs = timeNow;
    }

}

void BlackMagicCamera::setZoom() {
    if (okToSend(ZOOM)) {
        sdiCamera->writeCommandFixed16(
                cameraId,
                0,
                ZOOM_SPEED,
                0,
                speeds[ZOOM]
        );
        messageSent(ZOOM);
    }
}

void BlackMagicCamera::setFocus() {
    if (okToSend(FOCUS)) {
        sdiCamera->writeCommandFixed16(
                cameraId,
                0,
                FOC_POS,
                0,
                positions[FOCUS]
        );
        messageSent(FOCUS);
    }
}

void BlackMagicCamera::setIris() {
    if (okToSend(IRIS)) {
        sdiCamera->writeCommandFixed16(
                cameraId,
                0,
                IRIS_POS,
                0,
                positions[IRIS]
        );
        messageSent(IRIS);
    }
}

void BlackMagicCamera::start() {}

void BlackMagicCamera::stop() {}

void BlackMagicCamera::launchTasks() {
    if (enabled) {
        commandThread.onRun(ControlCamera);
        commandThread.setInterval(100);
        masterThread->add(&commandThread);
    }
}

void BlackMagicCamera::setCameraId(unsigned char id) {
    cameraId = id;
}

unsigned char BlackMagicCamera::getCameraId() {
    return cameraId;
}

unsigned char *BlackMagicCamera::getCameraIdPtr() {
    return &cameraId;
}

void BlackMagicCamera::setSpeed(Function function, double speed) {
    actives[function] = true;
    speeds[function] = speed;
}

void BlackMagicCamera::setPosition(Function function, long position) {
    if (position != positions[function]) {
        actives[function] = true;
        float newPos = (((float) position / LONG_MAX) / 2.0f + 0.5f);
        positions[function] = newPos;
    }
}

long BlackMagicCamera::getPosition(Function function) {
    return (long) ((positions[function] - 0.5) * 2 * LONG_MAX);
}

long BlackMagicCamera::getUpperBound(Function function) {
    return cameraUpperBound[function];
}

long BlackMagicCamera::getLowerBound(Function function) {
    return cameraLowerBound[function];
}

bool BlackMagicCamera::setUpperBound(Function function) {
    if (speeds[function] == 0) {
        cameraUpperBound[function] = positions[function];
        return true;
    }
    return false;
}

bool BlackMagicCamera::setLowerBound(Function function) {
    if (speeds[function] == 0) {
        cameraLowerBound[function] = positions[function];
        return true;
    }
    return false;
}

void BlackMagicCamera::unsetUpperBound(Function function) {
    cameraUpperBound[function] = LONG_MAX;
}

void BlackMagicCamera::unsetLowerBound(Function function) {
    cameraLowerBound[function] = LONG_MIN;
}

BlackMagicCamera::BlackMagicCamera() {
    enabled = false;
}

BlackMagicCamera::BlackMagicCamera(ThreadController *master) {
    cameraId = BMSDI_DEFAULT_CAMERA_ID;
    enabled = true;
    sdiCamera = new BMD_SDICameraControl_I2C(BMSDI_SHIELD_ADDRESS);
    sdiCamera->begin();
    sdiCamera->setOverride(false);
    masterThread = master;

    everSent = false;
    lastSentTimeMs = 0;

    currentOperation = 0;

    for (int i = 0; i < BMSDI_NB_FUNCTIONS; ++i) {
        speeds[i] = 0;
        positions[i] = 0;
        cameraUpperBound[i] = 0;
        cameraLowerBound[i] = 0;
        waiters[i] = false;
        actives[i] = false;
    }
}
