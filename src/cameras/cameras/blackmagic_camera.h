#ifndef FONKY_BLACKMAGIC_CAMERA_H
#define FONKY_BLACKMAGIC_CAMERA_H

/*!
 * @file blackmagic_camera.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section CAMERA
 * @class BlackMagicCamera
 * @brief Blackmagic Design cameras driver
 * This class makes use of the BMDSDIControl library witch allows to send camera commands through SDI.
 */


#include <BMDSDIControl.h>
#include <ThreadController.h>
#include "utils/fonky_utils.h"

#define BMSDI_SHIELD_ADDRESS 0x6E

#define BMSDI_DEFAULT_CAMERA_ID 7
#define BMSDI_NB_FUNCTIONS 3
#define BMSDI_TIME_SLEEP 30     //! 30ms between commands avoids system to be overloaded


class BlackMagicCamera {
public:
    //! Defines Blackmagic cameras functions
    enum Function {
        ZOOM, FOCUS, IRIS
    };

protected:
    //! Defines operations to send with SDI command
    enum Operation {
        FOC_POS,
        FOC_AUTO,
        IRIS_POS_PRO,
        IRIS_POS,
        IRIS_AUTO,
        IRIS_ORDINAL,
        IMAGE_STAB,
        ZOOM_POS_MM,
        ZOOM_POS,
        ZOOM_SPEED
    };

    //! The SDI camera object
    static BMD_SDICameraControl_I2C *sdiCamera;

    //! The system scheduler
    static ThreadController *masterThread;
    //! The camera command thread
    static Thread commandThread;

    //! Allows first message to be sent
    static bool everSent;

    //! Time elapsed between 2 commands
    static unsigned long lastSentTimeMs;

    static float positions[BMSDI_NB_FUNCTIONS];
    static float speeds[BMSDI_NB_FUNCTIONS];

    //! Functions waiting to be sent
    static bool waiters[BMSDI_NB_FUNCTIONS];
    //! Functions ready to called
    static bool actives[BMSDI_NB_FUNCTIONS];

    static long cameraUpperBound[BMSDI_NB_FUNCTIONS];
    static long cameraLowerBound[BMSDI_NB_FUNCTIONS];

    static unsigned char cameraId;

    //! Is the camera active now?
    static bool enabled;

    //! Current operation to be treated
    static unsigned char currentOperation;

    //! This method try to send current operation and go to the next operation (in a cycle)
    static void ControlCamera();

    static void setZoom();

    static void setFocus();

    static void setIris();

    static bool okToSend(Function function);

    //! Resets function states
    static void messageSent(Function function);

public:
    //! @see : Camera#start
    void start();

    //! @see : Camera#stop
    void stop();

    //! Sets up the camera command thread
    void launchTasks();

    void setCameraId(unsigned char id);

    unsigned char getCameraId();

    unsigned char *getCameraIdPtr();

    //! @see : Camera#setSpeed
    void setSpeed(Function function, double speed);

    //! @see : Camera#setPosition
    void setPosition(Function function, long position);

    //! @see : Camera#getPosition
    long getPosition(Function function);

    //! @see : Camera#getUpperBound
    long getUpperBound(Function function);

    //! @see : Camera#getLowerBound
    long getLowerBound(Function function);

    //! @see : Camera#setUpperBound
    bool setUpperBound(Function function);

    //! @see : Camera#setLowerBound
    bool setLowerBound(Function function);

    //! @see : Camera#unsetUpperBound
    void unsetUpperBound(Function function);

    //! @see : Camera#unsetLowerBound
    void unsetLowerBound(Function function);

    //! BlackMagicCamera default constructor
    BlackMagicCamera();

    /*!
     * BlackMagicCamera constructor
     * @param master the system scheduler
     */
    BlackMagicCamera(ThreadController *master);
};

#endif //FONKY_BLACKMAGIC_CAMERA_H
