#ifndef FONKY_NETWORK_CAMERA_TEMPLATE_H
#define FONKY_NETWORK_CAMERA_TEMPLATE_H

/*!
 * @file fonky_networkcamera_template.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section ABSTRACT
 * @class Camera
 * @brief Abstract class that defines a basic template for network camera classes.
 * This class inherits from Camera adds network functionnalities
 */

#include "fonky_camera_template.h"
#include "../../network/fonky_network.h"

class NetworkCamera : public Camera {

protected:
public:

    virtual void setIp(char ipAddress[4]) = 0;

    virtual IPAddress getIp() = 0;
};

#endif //FONKY_NETWORK_CAMERA_TEMPLATE_H