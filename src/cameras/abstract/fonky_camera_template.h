#ifndef FONKY_CAMERA_TEMPLATE_H
#define FONKY_CAMERA_TEMPLATE_H

/*!
 * @file fonky_camera_template.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section ABSTRACT
 * @class Camera
 * @brief Abstract class that defines a basic template for camera classes.
 */

#define CAMERA_NB_FUNCTIONS 6

#include "../../utils/fonky_utils.h"

class Camera {

protected:
    //! The camera displayed name
    const char* name;

    //! The index of the actual controller bound to the camera. Allows to switch cameras and controllers easily
    unsigned char controllerIndex;

public:
    //! Defines the 5 main PTZ cameras functions
    enum Function {
        PAN, TILT, ZOOM, FOCUS, IRIS, ALLFUNCT
    };

    //! Defines the 5 main PTZ cameras functions
    static const char *functionNames[CAMERA_NB_FUNCTIONS];

    const char *getName();
    unsigned char getControllerIndex();

    //! Pure virtual method to start camera
    virtual void    start() = 0;

    //! Pure virtual method to stop camera
    virtual void    stop() = 0;

    //! Pure virtual method to make the evolve a certain function at a certain speed
    virtual void    setSpeed(Function function, double speed) = 0;

    //! Pure virtual method to make the evolve a certain function up to a certain position
    virtual void    setPosition(Function function, long position) = 0;

    virtual long getPosition(Function function) = 0;

    //! Pure virtual method to lock the movement up to this upper bound
    virtual bool    setUpperBound(Function function) = 0;

    //! Pure virtual method to lock the movement down to this lower bound
    virtual bool    setLowerBound(Function function) = 0;

    //! Pure virtual method to unlock upper bound
    virtual void unsetUpperBound(Function function) = 0;

    //! Pure virtual method to unlock lower bound
    virtual void unsetLowerBound(Function function) = 0;

    virtual long getUpperBound(Function function) = 0;
    virtual long getLowerBound(Function function) = 0;

};

#endif //FONKY_NETWORK_CAMERA_TEMPLATE_H