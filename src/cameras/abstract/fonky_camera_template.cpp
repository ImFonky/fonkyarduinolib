#include "fonky_camera_template.h"

const char *Camera::functionNames[CAMERA_NB_FUNCTIONS] = {"PAN", "TILT", "ZOOM", "FOCUS", "IRIS"};

const char *Camera::getName() {
    return name;
}

unsigned char Camera::getControllerIndex() {
    return controllerIndex;
}
