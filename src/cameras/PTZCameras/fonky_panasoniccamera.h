#ifndef FONKY_PANASONICCAMERA_H
#define FONKY_PANASONICCAMERA_H

#include <ThreadController.h>

#include "../abstract/fonky_networkcamera_template.h"

#define PANCAM_DEFAULT_PORT 80
#define PANCAM_DEFAULT_IP {192,168,1,246};


class PanasonicCamera : public NetworkCamera {
protected:

    static Network *network;

    static unsigned char nbWaiters;

    static float positions[CAMERA_NB_FUNCTIONS];
    static float speeds[CAMERA_NB_FUNCTIONS];
    static long cameraUpperBound[CAMERA_NB_FUNCTIONS];
    static long cameraLowerBound[CAMERA_NB_FUNCTIONS];

    //! This method try to send current operation and go to the next operation (in a cycle)

    static void controlPanasonic(PanasonicCamera *camera, Function function, EthernetClient *client);

    static void controlPan(EthernetClient *client);

    static void controlTilt(EthernetClient *client);

    static void controlZoom(EthernetClient *client);

    static void controlFocus(EthernetClient *client);

    static void controlIris(EthernetClient *client);


public:

    static PanasonicCamera *currentCamera;

    IPAddress cameraIp;

    //! @see Camera#start
    void start();

    //! @see Camera#stop
    void stop();

    //! Sets up the camera command thread
    void launchTasks();

    //! @see Camera#unsetUpperBound
    void setSpeed(Function function, double speed);

    //! @see Camera#unsetUpperBound
    void setPosition(Function function, long position);

    long getPosition(Function function);

    long getUpperBound(Function function);

    long getLowerBound(Function function);

    //! @see Camera#unsetUpperBound
    bool setUpperBound(Function function);

    //! @see Camera#unsetUpperBound
    bool setLowerBound(Function function);

    //! @see Camera#unsetUpperBound
    void unsetUpperBound(Function function);

    //! @see Camera#unsetUpperBound
    void unsetLowerBound(Function function);

    //! @see NetworkCamera#setIp
    void setIp(char ipAddress[4]);

    //! @see NetworkCamera#getIp
    IPAddress getIp();

    /*!
     * PanasonicCamera constructor
     * @param network
     */
    PanasonicCamera(Network *network);

};


#endif //FONKY_PANASONICCAMERA_H
