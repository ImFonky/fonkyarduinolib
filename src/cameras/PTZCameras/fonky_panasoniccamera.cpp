//
// Created by pbmon on 27.07.2018.
//

#include "fonky_panasoniccamera.h"
#include "limits.h"


unsigned char PanasonicCamera::nbWaiters;

float PanasonicCamera::positions[CAMERA_NB_FUNCTIONS];
float PanasonicCamera::speeds[CAMERA_NB_FUNCTIONS];
long PanasonicCamera::cameraUpperBound[CAMERA_NB_FUNCTIONS];
long PanasonicCamera::cameraLowerBound[CAMERA_NB_FUNCTIONS];

PanasonicCamera *PanasonicCamera::currentCamera;

void PanasonicCamera::controlPanasonic(PanasonicCamera *camera, Function function, EthernetClient *client) {
    if (camera != nullptr && client != nullptr) {
        if ((nbWaiters > 0) && client->connect(camera->cameraIp, PANCAM_DEFAULT_PORT)) {
            SERIAL_PRINTLN(F("connected"));
            client->print("GET /cgi-bin/aw_ptz?cmd=#");
            switch (function) {
                case PAN:
                    client->print("P");
                    break;
                case TILT:
                    client->print("T");
                    break;
                case ZOOM:
                    client->print("Z");
                    break;
                case FOCUS:
                    client->print("F");
                    break;
                case IRIS:
                    client->print("AXI");
                    break;
                default:
                    SERIAL_PRINTLN(F("unknown function"));
            }

            client->print((unsigned char) ((speeds[function] * 50) + 50));
            client->println("&res=1 HTTP/1.0");
            client->print("Host: ");
            client->print(camera->cameraIp[0]);
            client->print(".");
            client->print(camera->cameraIp[1]);
            client->print(".");
            client->print(camera->cameraIp[2]);
            client->print(".");
            client->println(camera->cameraIp[3]);
            client->println("Connection: close");
            client->println();


            --nbWaiters;
            client->flush();
            client->stop();


        } else {
            SERIAL_PRINTLN(F("connection failed"));
        }
    }
}

void PanasonicCamera::controlPan(EthernetClient *client) {
    controlPanasonic(currentCamera, PAN, client);
}

void PanasonicCamera::controlTilt(EthernetClient *client) {
    controlPanasonic(currentCamera, TILT, client);
}

void PanasonicCamera::controlZoom(EthernetClient *client) {
    controlPanasonic(currentCamera, ZOOM, client);
}

void PanasonicCamera::controlFocus(EthernetClient *client) {
    controlPanasonic(currentCamera, FOCUS, client);
}

void PanasonicCamera::controlIris(EthernetClient *client) {
    controlPanasonic(currentCamera, IRIS, client);
}


void PanasonicCamera::start() {}

void PanasonicCamera::stop() {}

void PanasonicCamera::setSpeed(Function function, double speed) {
    if (speeds[function] != speed) {
        nbWaiters++;
    }

    speeds[function] = (float) speed;
}

void PanasonicCamera::setPosition(Function function, long position) {
    if (positions[function] != position) {
        nbWaiters++;
    }

    if (position != positions[function]) {
        float newPos = (((float) position / LONG_MAX) / 2.0f + 0.5f);
        positions[function] = newPos;
    }
}

long PanasonicCamera::getPosition(Function function) {
    return (long) ((positions[function] - 0.5) * 2 * LONG_MAX);
}

long PanasonicCamera::getUpperBound(Function function) {
    return cameraUpperBound[function];
}

long PanasonicCamera::getLowerBound(Function function) {
    return cameraLowerBound[function];
}

bool PanasonicCamera::setUpperBound(Function function) {
    if (speeds[function] == 0) {
        cameraUpperBound[function] = positions[function];
        return true;
    }
    return false;
}

bool PanasonicCamera::setLowerBound(Function function) {
    if (speeds[function] == 0) {
        cameraLowerBound[function] = positions[function];
        return true;
    }
    return false;
}

void PanasonicCamera::unsetUpperBound(Function function) {
    cameraUpperBound[function] = LONG_MAX;
}

void PanasonicCamera::unsetLowerBound(Function function) {
    cameraLowerBound[function] = LONG_MIN;
}

void PanasonicCamera::setIp(char ipAddress[4]) {
    cameraIp[0] = ipAddress[0];
    cameraIp[1] = ipAddress[1];
    cameraIp[2] = ipAddress[2];
    cameraIp[3] = ipAddress[3];
}

IPAddress PanasonicCamera::getIp() {
    return cameraIp;
}

PanasonicCamera::PanasonicCamera(Network *network) {
    cameraIp = PANCAM_DEFAULT_IP;
    nbWaiters = 0;
    currentCamera = this;

    for (int i = 0; i < CAMERA_NB_FUNCTIONS; ++i) {
        speeds[i] = 0;
        positions[i] = 0;
        cameraUpperBound[i] = 0;
        cameraLowerBound[i] = 0;
    }

    network->addCameraJob(&controlPan);
    network->addCameraJob(&controlTilt);
    network->addCameraJob(&controlZoom);
    network->addCameraJob(&controlFocus);
    network->addCameraJob(&controlIris);
}
