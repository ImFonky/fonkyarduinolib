#include "kesslercrane_blackmagic_camera.h"

void KesslerCraneBlackMagicCamera::start() {
    motor.start();
}

void KesslerCraneBlackMagicCamera::stop(){
    motor.stop();
    camera.stop();
}

void KesslerCraneBlackMagicCamera::activateCamera() {
    camera = BlackMagicCamera(masterThread);
    camera.launchTasks();
}

void KesslerCraneBlackMagicCamera::activateHead() {
    motor.launchTasks();
}

void KesslerCraneBlackMagicCamera::setSpeed(Function function, double speed){
    switch(function){
        case PAN:
            return motor.setSpeed(KesslercraneRevhead::PAN,speed);
        case TILT:
            return motor.setSpeed(KesslercraneRevhead::TILT,speed);
        case ZOOM:
            return camera.setSpeed(BlackMagicCamera::ZOOM, speed);
        case FOCUS:
        case IRIS:
        default:
            SERIAL_PRINTLN(F("Unknown function"));
    }
}

void    KesslerCraneBlackMagicCamera::setPosition(Function function, long position){
    switch(function){
        case PAN:
        case TILT:
            return SERIAL_PRINTLN(F("Unknown function"));
        case ZOOM:
            return camera.setPosition(BlackMagicCamera::ZOOM, position);
        case FOCUS:
            return camera.setPosition(BlackMagicCamera::FOCUS, position);
        case IRIS:
            return camera.setPosition(BlackMagicCamera::IRIS, position);
        default:
            SERIAL_PRINTLN(F("Unknown function"));
    }
}

long KesslerCraneBlackMagicCamera::getPosition(Function function) {
    switch(function){
        case PAN:
            return motor.getPosition(KesslercraneRevhead::PAN);
        case TILT:
            return motor.getPosition(KesslercraneRevhead::TILT);
        case ZOOM:
            return camera.getPosition(BlackMagicCamera::ZOOM);
        case FOCUS:
            return camera.getPosition(BlackMagicCamera::FOCUS);
        case IRIS:
            return camera.getPosition(BlackMagicCamera::IRIS);
        default:
            SERIAL_PRINTLN(F("Unknown function"));
            return 0;
    }
}

long KesslerCraneBlackMagicCamera::getUpperBound(Function function) {
    switch (function) {
        case PAN:
            return motor.getUpperBound(KesslercraneRevhead::PAN);
        case TILT:
            return motor.getUpperBound(KesslercraneRevhead::TILT);
        case ZOOM:
            return camera.getUpperBound(BlackMagicCamera::ZOOM);
        case FOCUS:
            return camera.getUpperBound(BlackMagicCamera::FOCUS);
        case IRIS:
            return camera.getUpperBound(BlackMagicCamera::IRIS);
        default:
            SERIAL_PRINTLN(F("Unknown function"));
            return 0;
    }
}

long KesslerCraneBlackMagicCamera::getLowerBound(Function function) {
    switch (function) {
        case PAN:
            return motor.getLowerBound(KesslercraneRevhead::PAN);
        case TILT:
            return motor.getLowerBound(KesslercraneRevhead::TILT);
        case ZOOM:
            return camera.getLowerBound(BlackMagicCamera::ZOOM);
        case FOCUS:
            return camera.getLowerBound(BlackMagicCamera::FOCUS);
        case IRIS:
            return camera.getLowerBound(BlackMagicCamera::FOCUS);
        default:
            SERIAL_PRINTLN(F("Unknown function"));
            return 0;
    }
}

bool    KesslerCraneBlackMagicCamera::setUpperBound(Function function){
    switch(function){
        case PAN:
            return motor.setUpperBound(KesslercraneRevhead::PAN);
        case TILT:
            return motor.setUpperBound(KesslercraneRevhead::TILT);
        case ZOOM:
            return camera.setUpperBound(BlackMagicCamera::ZOOM);
        case FOCUS:
            return camera.setUpperBound(BlackMagicCamera::FOCUS);
        case IRIS:
            return camera.setUpperBound(BlackMagicCamera::IRIS);
        default:
            SERIAL_PRINTLN(F("Unknown function"));
            return false;
    }
}

bool    KesslerCraneBlackMagicCamera::setLowerBound(Function function){
    switch(function){
        case PAN:
            return motor.setLowerBound(KesslercraneRevhead::PAN);
        case TILT:
            return motor.setLowerBound(KesslercraneRevhead::TILT);
        case ZOOM:
            return camera.setLowerBound(BlackMagicCamera::ZOOM);
        case FOCUS:
            return camera.setLowerBound(BlackMagicCamera::FOCUS);
        case IRIS:
            return camera.setLowerBound(BlackMagicCamera::IRIS);
        default:
            SERIAL_PRINTLN(F("Unknown function"));
            return false;
    }
}

void KesslerCraneBlackMagicCamera::unsetUpperBound(Function function) {
    switch (function) {
        case PAN:
            return motor.unsetUpperBound(KesslercraneRevhead::PAN);
        case TILT:
            return motor.unsetUpperBound(KesslercraneRevhead::TILT);
        case ZOOM:
            return camera.unsetUpperBound(BlackMagicCamera::ZOOM);
        case FOCUS:
            return camera.unsetUpperBound(BlackMagicCamera::FOCUS);
        case IRIS:
            return camera.unsetUpperBound(BlackMagicCamera::IRIS);
        default:
            SERIAL_PRINTLN(F("Unknown function"));
    }
}

void KesslerCraneBlackMagicCamera::unsetLowerBound(Function function) {
    switch (function) {
        case PAN:
            return motor.unsetLowerBound(KesslercraneRevhead::PAN);
        case TILT:
            return motor.unsetLowerBound(KesslercraneRevhead::TILT);
        case ZOOM:
            return camera.unsetLowerBound(BlackMagicCamera::ZOOM);
        case FOCUS:
            return camera.unsetLowerBound(BlackMagicCamera::FOCUS);
        case IRIS:
            return camera.unsetLowerBound(BlackMagicCamera::IRIS);
        default:
            SERIAL_PRINTLN(F("Unknown function"));
    }
}

void KesslerCraneBlackMagicCamera::setCameraId(unsigned char id) {
    camera.setCameraId(id);
}

unsigned char KesslerCraneBlackMagicCamera::getCameraId() {
    return camera.getCameraId();
}

unsigned char *KesslerCraneBlackMagicCamera::getCameraIdPtr() {
    return camera.getCameraIdPtr();
}



KesslerCraneBlackMagicCamera::KesslerCraneBlackMagicCamera(){}

KesslerCraneBlackMagicCamera::KesslerCraneBlackMagicCamera(ThreadController *master){
    name = "Kessler/BlackmagicPTZ";
    controllerIndex = 0;
    masterThread = master;
    motor = KesslercraneRevhead(master,40,41,30,31,6,12,0,0,KesslercraneRevhead::PAN,KesslercraneRevhead::TILT);
    activateHead();
}