#ifndef FONKY_PT_KESSLERCRANE_ZFI_BLACKMAGIC_H
#define FONKY_PT_KESSLERCRANE_ZFI_BLACKMAGIC_H

/*!
 * @file kesslercrane_blackmagic_camera.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section CAMERA
 * @class KesslerCraneBlackMagicCamera
 * @brief Rev Head and Blackmagic driver
 * Combines the KesslercraneRevhead and the BlackMagicCamera to make a PTZ camera
 */

#include "../abstract/fonky_camera_template.h"
#include "../heads/kesslercrane_revhead.h"
#include "../cameras/blackmagic_camera.h"

#include <ThreadController.h>

class KesslerCraneBlackMagicCamera : public Camera {

protected:
    //! The Rev Head (Mecanic part)
    KesslercraneRevhead motor;
    //! The Blackmagic camera (Lenses part)
    BlackMagicCamera camera;

    //! The system scheduler
    ThreadController *masterThread;

public:

    //! @see Camera#start
    void start();

    //! @see Camera#stop
    void stop();

    //! Activates separately Blackmagic camera
    void activateCamera();

    //! Activates separately Kessler Rev Head
    void activateHead();

    //! @see Camera#unsetUpperBound
    void    setSpeed(Function function, double speed);

    //! @see Camera#unsetUpperBound
    void    setPosition(Function function, long position);

    long getPosition(Function function);

    long getUpperBound(Function function);

    long getLowerBound(Function function);

    //! @see Camera#unsetUpperBound
    bool    setUpperBound(Function function);

    //! @see Camera#unsetUpperBound
    bool    setLowerBound(Function function);

    //! @see Camera#unsetUpperBound
    void unsetUpperBound(Function function);

    //! @see Camera#unsetUpperBound
    void unsetLowerBound(Function function);

    //! @see BlackMagicCamera#setCameraId
    void setCameraId(unsigned char id);

    //! @see BlackMagicCamera#getCameraId
    unsigned char getCameraId();

    //! @see BlackMagicCamera#getCameraIdPtr
    unsigned char *getCameraIdPtr();


    //! KesslerCraneBlackMagicCamera default constructor
    KesslerCraneBlackMagicCamera();

    /*!
     * KesslerCraneBlackMagicCamera constructor
     * @param master the system scheduler
     */
    KesslerCraneBlackMagicCamera(ThreadController *master);
};


#endif //FONKY_PT_KESSLERCRANE_ZFI_BLACKMAGIC_H
