#ifndef FONKY_DEVICECONTROLLER_H
#define FONKY_DEVICECONTROLLER_H

/*!
 * @file fonky_devicecontroller.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section CONTROLLER
 * @class DeviceController
 * @brief This class implements the embedded controller.
 */

#include "abstract/fonky_controller_template.h"

#define INIT_DEVICE_SPEED 0.2
#define INIT_DEVICE_POSITION 0

class DeviceController : public Controller {
protected:
    //! Constant speed of each function
    double speeds[CAMERA_NB_FUNCTIONS];
public:
    //! DeviceController default constructor
    DeviceController();

    /*!
     * DeviceController constructor
     * @param camera the managed camera
     * @param index the index of the controller in the system
     */
    DeviceController(Camera *camera, unsigned char index);

    //! Starts controller and camera
    void    start();

    //! Stops controller and camera
    void    stop();

    //! Wraps camera method @see Camera#setSpeed
    void    setSpeed(Function function, double speed);

    //! Wraps camera method @see Camera#setPosition
    void    setPosition(Function function, long position);

    //! Wraps camera method @see Camera#getPosition
    long getPosition(Function function);

    //! Wraps camera method @see Camera#getUpperBound
    long getUpperBound(Function function);

    //! Wraps camera method @see Camera#getLowerBound
    long getLowerBound(Function function);

    //! Wraps camera method @see Camera#setUpperBound
    bool    setUpperBound(Function function);

    //! Wraps camera method @see Camera#setLowerBound
    bool    setLowerBound(Function function);

    //! Wraps camera method @see Camera#unsetUpperBound
    void unsetUpperBound(Function function);

    //! Wraps camera method @see Camera#unsetLowerBound
    void unsetLowerBound(Function function);

    void setCamera(Camera *camera);
    Camera *getCamera();

    /*!
     * @brief This functions allows to set constant speeds witch is needed in the case of the device embedded controller.
     * Buttons clicks events doesnt give precisely the pressure on the screen so the speed is constant.
     * @see SpeedSelectionNode
     * @param function the related camera function
     * @param speedPower the speed to be applied when click on button
     */
    void setSpeedPower(Function function, double speedPower);
    double getSpeedPower(Function function);
};


#endif //FONKY_DEVICECONTROLLER_H
