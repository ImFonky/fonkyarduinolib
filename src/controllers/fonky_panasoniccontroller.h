#ifndef FONKY_PANASONICCONTROLLER_H
#define FONKY_PANASONICCONTROLLER_H

/*!
 * @file fonky_panasoniccontroller.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section CONTROLLER
 * @class PanasonicController
 * @brief This class implements a panasonic AW-RP50 controller manager.
 * This class makes use of the server side of the Network class to interpret HTTP messages from controller.
 */


#include "fonky_devicecontroller.h"
#include "../network/fonky_network.h"

#define NETWORK_URL_BUFFER_SIZE 64
#define NB_CAMERA_COMMANDS 35
#define NB_PTZ_COMMANDS 27

class PanasonicController : public DeviceController {
public:
    //! Defines URL rootpaths
    enum Roots {
        NOROOT, CGI_BIN, LIVE
    };
    //! Defines /cgi-bin/ paths
    enum CGIPath {
        NOPATH, AW_PTZ, AW_CAM, SESSION, EVENT
    };
    //! Defines predefined commands
    enum Command {
        NOCOM, O, AWLPC1, QFT, PARSED
    };

    //! Defines all the camera commands sent by AW-RP50
    enum CameraCommand {
        CAM_UNKNOWN,
        GAIN_UP,
        GAIN_SELECT,
        T_PEDESTAL_P,
        T_PEDESTAL_D,
        RI_GAIN,
        BI_GAIN,
        R_PEDESTAL,
        B_PEDESTAL,
        AWC_MODE,
        AWB_SET,
        ABB_SET,
        SHUTTER,
        DETAIL,
        HE870_HD_DETAIL,
        SCENE_FILE,
        COLOR_BAR_CAMERA,
        CAM_ND_CONTROL,
        MENU_OFF_ON,
        MENU_SW,
        ITEM_SW,
        YES_SW,
        NO_SW,
        CAM_AUTO_FOCUS,
        CAM_AUTO_IRIS,
        R_GAIN,
        B_GAIN,
        SHUTTER_MODE,
        SHUTTER_SPEED,
        TOTAL_DTL_LEVEL,
        HC1500_SD_DETAIL,
        HC1500_SD_DETAIL_LVL,
        PRESET_SCOPE,
        PUSH_AUTO_FOCUS,
        CONTRAST
    };

    //! Defines all the PTZ commands sent by AW-RP50
    enum PTZCommand {
        PTZ_UNKNOWN,
        SPEED_WITH_ZOOM,
        PAN_PRESET_SPEED,
        TILT_PRESET_SPEED,
        PTZ_ND_CONTROL,
        LAMP_CONTROL,
        OPTION_SW_CONTROL,
        DEFROSTER_CONTROL,
        WIPER_CONTROL,
        TEMP_CONTROL,
        INSTALL_POSITION,
        PAN_SPEED_CONTROL,
        TILT_SPEED_CONTROL,
        FOCUS_SPEED_CONTROL,
        ZOOM_SPEED_CONTROL,
        IRIS_CONTROL,
        PTZ_AUTO_IRIS,
        PAN_TILT_SPEED,
        POWER,
        PRESET_RECALL,
        PRESET_MEMORY,
        PRESET_DELETE,
        LIMIT_SETTINGS,
        LIMIT_C_SETTINGS,
        HOME_POSITION,
        PT_AP_CONTROL,
        PTZ_AUTO_FOCUS
    };
protected:

    //! Last speeds are memorized to avoid sending same speed multiple time. This improve performances and stability.
    double speeds[CAMERA_NB_FUNCTIONS];

    //! All the camera commands string tokens
    static const char *strCameraCommands[NB_CAMERA_COMMANDS];
    //! All the PTZ commands string tokens
    static const char *strPTZCommands[NB_PTZ_COMMANDS];

    //! The network class providing server functions
    static Network *network;

    //! The network class providing server functions
    static char buffer[NETWORK_URL_BUFFER_SIZE];

    //! PanasonicController singleton. Points to the last PanasonicController object created.
    static PanasonicController *activeController;

    //! Predefined answer for LiveRoot request
    static void HandleLiveRoot(EthernetClient *client);

    //! Predefined answer for Session request
    static void HandleSession(EthernetClient *client);

    //! Predefined answer for Event request
    static void HandleEvent(EthernetClient *client);

    //! Predefined answers for known commands @see PanasonicController#Command
    static void HandleAW(EthernetClient *client, Command *cmd);

    //! Camera command parser
    static void ParseCommand(EthernetClient *client);

    //! Camera command executor
    static void DispatchPTZCommands(PTZCommand ptzCmd, unsigned short value);

    //! Main request parser. Dispatches to Handle methods when pattern found.
    static void ParseURL(EthernetClient *client);

public:
    //! PanasonicController default constructor
    PanasonicController();

    /*!
     * PanasonicController constructor
     * @param cam the camera to be controlled
     * @param index the index of the controller in the system
     * @param network the helper class that allows TCP/HTTP request management
     */
    PanasonicController(Camera *cam, unsigned char index, Network *network);
};


#endif //FONKY_PANASONICCONTROLLER_H
