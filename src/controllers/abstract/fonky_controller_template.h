#ifndef FONKY_CONTROLLER_TEMPLATE_H
#define FONKY_CONTROLLER_TEMPLATE_H

/*!
 * @file fonky_controller_template.h
 * @author P-B Monaco
 * @date 25.07.2018
 * @version 1.0
 * @section ABSTRACT
 * @class Controller
 * @brief Abstract class that defines a basic template for controller classes.
 * This class inherits from Camera and most of the inherited methods will wrap Camera homologous methods. Data may also be formatted in the wrapper.
 * @see Camera
 */

#include "../../cameras/abstract/fonky_camera_template.h"

class Controller : public Camera {
protected:
    //! The controlled camera
    Camera *camera;

public:

    virtual void setCamera(Camera *camera) = 0;

    virtual Camera *getCamera() = 0;

    //! @see DeviceController#setSpeedPower
    virtual void setSpeedPower(Function function, double speedPower) = 0;

    //! @see DeviceController#getSpeedPower
    virtual double getSpeedPower(Function function) = 0;
};


#endif //FONKY_CONTROLLER_TEMPLATE_H
