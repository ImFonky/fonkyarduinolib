//
// Created by pbmon on 17.07.2018.
//

#include "fonky_panasoniccontroller.h"
#include "../utils/fonky_tools.h"

Network *PanasonicController::network;
char PanasonicController::buffer[NETWORK_URL_BUFFER_SIZE];
PanasonicController *PanasonicController::activeController;

const char *PanasonicController::strCameraCommands[NB_CAMERA_COMMANDS] = {
        "ER1:",
        "OGU",
        "OGS",
        "OTP",
        "OTD",
        "ORI",
        "OBI",
        "ORP",
        "OBP",
        "OAW",
        "OWS",
        "OAS",
        "OSH",
        "ODT",
        "OHD",
        "XSF",
        "DCB",
        "OFT",
        "DUS",
        "DPG",
        "DIT",
        "DUP",
        "DDW",
        "OAF",
        "ORS",
        "ORG",
        "OBG",
        "OSA:90",
        "OSA:91",
        "OSA:30",
        "OSE:0E",
        "OSE:00",
        "OSE:71",
        "OSE:69",
        "OSD:48"
};

const char *PanasonicController::strPTZCommands[NB_PTZ_COMMANDS] = {
        "ER1:",
        "SWZ",
        "UPVS",
        "UTVS",
        "D2",
        "D4",
        "D6",
        "D7",
        "D8",
        "D9",
        "INS",
        "P",
        "T",
        "F",
        "Z",
        "AXI",
        "D3",
        "PTS",
        "O",
        "R",
        "M",
        "C",
        "L",
        "LC",
        "U",
        "APC",
        "D1"
};

void PanasonicController::HandleLiveRoot(EthernetClient *client) {
    // Header
//    Serial.println(F("HandleLiveRoot"));
    client->println(F("HTTP/1.0 200 OK"));
    client->println(F("Cache-Control: no-cache, no-store"));
    client->println(F("Status: 200"));
    client->println(F("Connection: close"));
    client->println(F("Content-Type: text/plain"));
    client->println(F("Set-Cookie: Session=0"));
    client->println(F("Accept-Ranges: bytes"));
    client->println(F("Content-length: 397"));
    client->println(F("Date: Fri, 02 Jan 2015 22:44:35 GMT"));
    client->println();
    // Payload
    client->println(F("p1"));
    client->println(F("OID:AW-UE70"));
    client->println(F("CGI_TIME:0"));
    client->println(F("OSA:87:0x15"));
    client->println(F("TITLE:Fonky device"));
    client->println(F("OSD:B9:0x15"));
    client->println(F("OGU:0x80"));
    client->println(F("OTD:0x1E"));
    client->println(F("OAW:0"));
    client->println(F("OSH:0x0"));
    client->println(F("ODT:1"));
    client->println(F("OSF:0"));
    client->println(F("OBR:0"));
    client->println(F("sWZ1"));
    client->println(F("OSE:71:0"));
    client->println(F("iNS0"));
    client->println(F("OUS:0"));
    client->println(F("d10"));
    client->println(F("d30"));
    client->println(F("s0"));
    client->println(F("OSA:30:89"));
    client->println(F("d20"));
    client->println(F("d60"));
    client->println(F("d40"));
    client->println(F("OSD:4F"));
    client->println(F("OER:0"));
    client->println(F("rt1"));
    client->println(F("axz555"));
    client->println(F("rER00"));
    client->println(F("axf96A"));
    client->println(F("pE000000000015"));
    client->println(F("pE010000000000"));
    client->println(F("pE020000000000"));
    client->println(F("uPVS500"));
    client->println(F("lC10"));
    client->println(F("lC20"));
    client->println(F("lC30"));
    client->println(F("lC40"));
    client->println(F("ORG:0"));
    client->println(F("OBG:0"));
    client->println(F("OSD:B1:0x008"));
    client->println(F("pST2"));
    client->println(F("pRF1"));
    client->println(F("OIS:0"));
    client->println(F("OSE:70:0"));
    client->println(F("OSD:B3:1"));
    client->println(F("ODE:0"));
    client->println();
}

void PanasonicController::HandleSession(EthernetClient *client) {
    // Header
//    Serial.println(F("HandleSession"));
    client->println(F("HTTP/1.1 200 OK"));
    client->println(F("Cache-Control: no-cache, no-store"));
    client->println(F("Status: 200"));
    client->println(F("Connection: close"));
    client->println(F("Content-Type: text/plain"));
    client->println(F("Transfer-Encoding: chunked"));
    client->println(F("Date: Fri, 02 Jan 2015 22:54:58 GMT"));
    client->println();
    // Payload
    client->println(F("Event session:0"));
    client->println();


    //Restarting motor in case of disconnecting or embedded stop
    activeController->start();
}

void PanasonicController::HandleEvent(EthernetClient *client) {
    // Header
//    Serial.println(F("HandleEvent"));
    client->println(F("HTTP/1.1 204 No Content"));
    client->println(F("Cache-Control: no-cache, no-store"));
    client->println(F("Status: 204"));
    client->println(F("Connection: close"));
    client->println(F("Set-Cookie: Session=0"));
    client->println(F("Accept-Ranges: bytes"));
    client->println(F("Date: Fri, 02 Jan 2015 22:54:58 GMT"));
    client->println();
}

void PanasonicController::HandleAW(EthernetClient *client, Command *cmd) {
//    Serial.println(F("HandleAW"));
    switch (*cmd) {
        case O:
            client->println(F("HTTP/1.0 200 OK"));
            client->println(F("Connection: close"));
            client->println(F("Content-Type: text/plain"));
            client->println(F("Set-Cookie: Session=0"));
            client->println(F("Accept-Ranges: bytes"));
            client->println(F("Cache-Control: no-cache"));
            client->println(F("Content-length: 2"));
            client->println(F("Date: Fri, 02 Jan 2015 22:54:58 GMT"));
            client->println();
            client->println(F("p1"));
            break;
        case AWLPC1:
            client->println(F("HTTP/1.0 200 OK"));
            client->println(F("Connection: close"));
            client->println(F("Content-Type: text/plain"));
            client->println(F("Set-Cookie: Session=0"));
            client->println(F("Accept-Ranges: bytes"));
            client->println(F("Cache-Control: no-cache"));
            client->println(F("Content-length: 4"));
            client->println(F("Date: Fri, 02 Jan 2015 22:54:58 GMT"));
            client->println();
            client->println(F("lPC1"));
            break;
        case QFT:
            client->println(F("HTTP/1.0 200 OK"));
            client->println(F("Connection: close"));
            client->println(F("Content-Type: text/plain"));
            client->println(F("Set-Cookie: Session=0"));
            client->println(F("Accept-Ranges: bytes"));
            client->println(F("Cache-Control: no-cache"));
            client->println(F("Content-length: 5"));
            client->println(F("Date: Fri, 02 Jan 2015 22:54:58 GMT"));
            client->println();
            client->println(F("OFT:0"));
            break;
        case NOCOM:
            *cmd = PARSED;
            ParseCommand(client);
            break;
        default:
            *cmd = PARSED;
            client->println(F("HTTP/1.0 404 Not Found"));
            client->println(F("Connection: close"));
            client->println(F("Status: 404"));
            break;
    }
}

void PanasonicController::ParseCommand(EthernetClient *client) {

    char *reader = buffer;
    char where = 0;
    bool cmd_found = false;
    reader += 4;
    if (*reader == '#') {
        ++reader;
        for (unsigned char i = 0; i < NB_PTZ_COMMANDS; ++i) {
            if ((where = fonky_strcmp(reader, strPTZCommands[i]))) {
                cmd_found = true;
                char *strValue = &reader[where];
                unsigned short value = 0;
                unsigned short offset = 0;

                if (strValue[2] >= '0' && strValue[2] <= '9') {
                    value += (strValue[0] - '0') * 100;
                    offset = 1;
                }
                value = (strValue[offset] - '0') * 10;
                value += (strValue[offset + 1] - '0');

                DispatchPTZCommands((PTZCommand) i, value);

                client->write('#');
                client->write(strPTZCommands[i]);
                client->write(strValue[0]);
                client->write(strValue[1]);
                if (offset) {
                    client->write(strValue[2]);
                }
                i = NB_PTZ_COMMANDS;
            }
        }
    } else {
        for (unsigned char i = 0; i < NB_CAMERA_COMMANDS; ++i) {
            if ((where = fonky_strcmp(reader, strCameraCommands[i]))) {
                cmd_found = true;
                char *value = &reader[where];
                // Answer
                client->write(strCameraCommands[i]);
                client->write(value);
            }
        }
    }
}

void PanasonicController::DispatchPTZCommands(PTZCommand ptzCmd, unsigned short value) {
    double dblValue = ((double) value - 50) / 50;
    if (activeController->speeds[0] != dblValue) {
        activeController->speeds[0] = dblValue;
        switch (ptzCmd) {
            case PAN_SPEED_CONTROL:
                return activeController->setSpeed(PAN, dblValue);
            case TILT_SPEED_CONTROL:
                return activeController->setSpeed(TILT, dblValue);
            case ZOOM_SPEED_CONTROL:
                return activeController->setSpeed(ZOOM, dblValue);
            case FOCUS_SPEED_CONTROL:
                return activeController->setSpeed(FOCUS, dblValue);
            case IRIS_CONTROL:
                return activeController->setSpeed(IRIS, dblValue);
            default:
                SERIAL_PRINTLN(F("Unknown function"));
        }
    }
}

void PanasonicController::ParseURL(EthernetClient *client) {
    bool currentLineIsBlank = true;

    buffer[0] = 0;
    buffer[NETWORK_URL_BUFFER_SIZE - 1] = 0;

    char *writer = buffer;
    unsigned char slashCounter = 0;
    bool last = false;
    unsigned char nbLast = 0;
    char c = 0;

    Roots root = NOROOT;
    CGIPath path = NOPATH;
    Command cmd = NOCOM;

//    DO_MEMORY_TEST();

    while (!last && client->connected() && (writer - buffer) < (NETWORK_URL_BUFFER_SIZE - 1)) {
        if (client->available()) {
            c = client->read();
            client->flush();

//            Serial.print(c);

            if (slashCounter > 0) {
                *writer = c;
                ++writer;
            }

            if (c == '/') {
                ++slashCounter;
            } else {
                if (path == NOPATH && c == '?') {
                    *writer = 0;
                    if (buffer[4] == 'p' && buffer[5] == 't' && buffer[6] == 'z') {
                        path = AW_PTZ;
                    } else if (buffer[4] == 'c' && buffer[5] == 'a' && buffer[6] == 'm') {
                        path = AW_CAM;
                    } else if (buffer[1] == 'm' && buffer[2] == 'a' && buffer[3] == 'n') {
                        path = SESSION;
                        HandleSession(client);
                    } else if (buffer[1] == 'e' && buffer[2] == 'v' && buffer[3] == 'e') {
                        path = EVENT;
                        HandleEvent(client);
                    }

                    writer = buffer;
                } else if (cmd == NOCOM && (c == '&' || c == 0)) {
                    *writer = 0;
                    if (buffer[0] == 'c' && buffer[1] == 'm' && buffer[2] == 'd' && buffer[3] == '=') {
                        if (buffer[4] == '#' && buffer[5] == 'O') {
                            cmd = O;
                        } else if (buffer[4] == 'Q' && buffer[5] == 'F' && buffer[6] == 'T') {
                            cmd = QFT;
                        } else if (buffer[4] == '#' && buffer[5] == 'L' && buffer[6] == 'P' && buffer[7] == 'C' &&
                                   buffer[8] == '1') {
                            cmd = AWLPC1;
                        }
                        HandleAW(client, &cmd);
                    }
                }
            }

            if (root == NOROOT && slashCounter == 2) {
                *writer = 0;

                if (buffer[0] == 'c' && buffer[1] == 'g' && buffer[2] == 'i') {
                    root = CGI_BIN;
                } else {
                    root = LIVE;
                    HandleLiveRoot(client);
                }
                *buffer = *(writer - 1);
                writer = buffer + 1;
            }
            if (c == '\n' && currentLineIsBlank) {
                client->flush();
                client->stop();
            }
            if (c == '\n') {
                currentLineIsBlank = true;
            } else if (c != '\r') {
                currentLineIsBlank = false;
            }
        }
    }
}

PanasonicController::PanasonicController() : DeviceController() {}

PanasonicController::PanasonicController(Camera *cam, unsigned char index, Network *net) :
        DeviceController(cam, index) {
    name = "Panasonic AW-RP50 controller";
    network = net;
    network->setControllerJob(&ParseURL);
    for (unsigned char i = 0; i < NETWORK_URL_BUFFER_SIZE; ++i) {
        buffer[i] = 0;
    }
    activeController = this;
    activeController->start();
}



