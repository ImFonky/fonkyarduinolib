//
// Created by pbmon on 10.07.2018.
//

#include "fonky_devicecontroller.h"

DeviceController::DeviceController(){}

DeviceController::DeviceController(Camera *cam, unsigned char index) {
    name = "Device controller";
    controllerIndex = index;
    camera = cam;

    ((DeviceController *) camera)->controllerIndex = controllerIndex;
    for(unsigned char t = 0; t < CAMERA_NB_FUNCTIONS; ++t){
        speeds[t] = INIT_DEVICE_SPEED;
    }
}

void    DeviceController::start(){
    camera->start();
}
void    DeviceController::stop(){
    camera->stop();
}

void    DeviceController::setSpeed(Function function, double speed){
    camera->setSpeed(function,speed);
}

void    DeviceController::setPosition(Function function, long position){
    camera->setPosition(function,position);
}

long DeviceController::getPosition(Function function) {
    return camera->getPosition(function);
}

long DeviceController::getUpperBound(Function function) {
    return camera->getUpperBound(function);
}

long DeviceController::getLowerBound(Function function) {
    return camera->getLowerBound(function);
}

bool    DeviceController::setUpperBound(Function function){
    return camera->setUpperBound(function);
}

bool    DeviceController::setLowerBound(Function function){
    return camera->setLowerBound(function);
}

void DeviceController::unsetUpperBound(Function function) {
    camera->unsetUpperBound(function);
}

void DeviceController::unsetLowerBound(Function function) {
    camera->unsetLowerBound(function);
}

void DeviceController::setCamera(Camera *cam) {
    camera->stop();
    camera = cam;
    camera->start();
}

Camera *DeviceController::getCamera() {
    return camera;
}

void DeviceController::setSpeedPower(Function function, double speedPower) {
    speeds[function] = speedPower;
}

double DeviceController::getSpeedPower(Function function) {
    return speeds[function];
}
