# PTZ-Camera universal controller 
__P-B Monaco__

## Description
This Arduino c++ library is designed for the follwing hardware package :  

* Arduino Mega 2560  
* Pololu Dual VNH5019 (Arduino Shield)  
* Blackmagic 3G-SDI (Arduino Shield)  
* Arduino Ethernet Shield  
* Arduino TFT Touch Shield  

This library offers the user a set of simple objects to implement motors, network, menu :  

* System `fonky_system.h` : Main class, launches the general controller with all the possible functions  
* Camera and NetworkCamera `fonky_camera_template.h` : Camera class template. Every new camera must inherit from this template in order to guarantee it usability with the controller part  
* Controller `fonky_controller_template.h` : Controller class template  
* Network `fonky_network.h` : Http client and server allowing to communicate with ethernet cameras and controllers  
* Menu `fonky_menu.h` : Menu class to allow user to modify system variables or control cameras directly  

The Menu class contains several Node classes that represents the elements of the menu :  

* `Node` : This is an abstract class for all Node classes. It contains a description of everything in a basic Node. A Button struct is also wrapped in it.  
* `MenuNode` : This node is a menu that contains other nodes. When there is too much elements to be displayed this node will automatically have up/down buttons to navigate.  
* `SelectionNode<T>` : This node is also navigable but the only allowed childrens are ValueNode.  
* `ValueNode<T>` : This node is never displayed, it contains only positions. SelectionNode and ValueNode are templated, so they can be used in many ways.  
* Other types of Node are used and you can see how by looking in the `menu/nodes/menunodes/` directory witch contains pre-filled MenuNodes.  

## Installation  

1. Download the [arduino IDE](https://www.arduino.cc/en/main/software)  
2. Download and install MemoryFree library from this site : https://playground.arduino.cc/Code/AvailableMemory  
3. Copy-paste BMDSDIControl library from Blackmagic Design 3G-SDI Arduino Shield CDrom. You can also find it on private git repos but without warrantly. (e.g. https://github.com/kasperskaarhoj/SKAARHOJ-Open-Engineering/tree/master/ArduinoLibs/BMDSDIControl)  
4. On the top menubar, select `Sketch->Import Library...` 
        ![addlibrary](./img/ArduinoIdeMenu.png)
        Then enter the library manager  
        ![libManagement](./img/LibManagement.png)  
5. Install DualVNH5019MotorShield 3.0  
6. Install ArduinoThread 2.1.1  
7. Install Touch Screen Driver  
8. Install TFT Touch Schield v2.0
9. Install Ethernet2 1.0.4  
10. Clone this repo into :  
    * Windows : %userprofile%\Documents\Arduino\libraries  
    * Linux : ~\Documents\Arduino\libraries  
11. Open Arduino IDE and check one of the examples under fonky_archi.  

## Versions
__Actual :__ 1.0

## Supported platforms :  

* Arduino Mega 2560  

### Usefull links
[Pololu Dual VNH5019 KesslercraneRevhead Driver Shield](https://github.com/pololu/dual-vnh5019-motor-shield)